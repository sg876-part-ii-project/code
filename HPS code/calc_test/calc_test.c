#define _BSD_SOURCE
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include "bridges.h"
#include "spamsum.h"

// 1 MiB
//#define MAX_BUFFER_SIZE (1 * 1024 * 1024)
// 1 kiB
#define MAX_BUFFER_SIZE (1024)
#define MIN_BUFFER_SIZE (256)
#define ITERATION_COUNT (128)

#define RESULT_OFFSET (0x00)
#define STREAM_OFFSET (0x84)
#define LENGTH_OFFSET (0x80)

#define STREAM_FIRST  (STREAM_OFFSET + 0)
#define STREAM_INTER  (STREAM_OFFSET + 1)
#define STREAM_FINAL  (STREAM_OFFSET + 2)
#define STREAM_LENGTH (LENGTH_OFFSET)

#define RES_OK    (RESULT_OFFSET + 0x00)
#define RES_BSN   (RESULT_OFFSET + 0x08)
#define RES_FSLEN (RESULT_OFFSET + 0x09)
#define RES_HSLEN (RESULT_OFFSET + 0x0A)
#define RES_FSDIG (RESULT_OFFSET + 0x10)
#define RES_HSDIG (RESULT_OFFSET + 0x50)

#define TOSECS / 1000000

const char* b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int rand_between(int geq, int lt) {
    if (geq == lt) return geq;
    return geq + (rand() % (lt - geq));
}

uint8_t read_byte(unsigned int offset) {
    uint8_t result = *(volatile uint8_t *) (h2f_hwb + offset);
    return result;
}

void write_byte(unsigned int offset, uint8_t byte) {
    *(volatile uint8_t *) (h2f_hwb + offset) = byte;
}

void write_halfword(unsigned int offset, uint16_t halfword) {
    *(volatile uint16_t *) (h2f_hwb + offset) = halfword;
}

void write_word(unsigned int offset, uint32_t word) {
    *(volatile uint32_t *) (h2f_hwb + offset) = word;
}

int min(int a, int b) {
    if (a > b) return b;
    return a;
}

char* get_spamsum_string(void) {
    char* str_buf = malloc(64 + 32 + 20);
    int next_char = snprintf(str_buf, 64+32+20, "%u:", 3 * (1 << read_byte(RES_BSN)));
    int fsdig_len = min(read_byte(RES_FSLEN) + 1, 64);
    for (int i = 0; i < fsdig_len; i++) {
        str_buf[next_char++] = b64[read_byte(RES_FSDIG + i)];
    }
    str_buf[next_char++] = ':';
    int hsdig_len = min(read_byte(RES_HSLEN) + 1, 64);
    for (int i = 0; i < hsdig_len; i++) {
        str_buf[next_char++] = b64[read_byte(RES_HSDIG + i)];
    }
    str_buf[next_char++] = '\0';
    return str_buf;
}

void print_time_diff(struct timeval end, struct timeval begin) {
    struct timeval diff; timersub(&end, &begin, &diff);
    printf("%ld.%06ld\n", diff.tv_sec, diff.tv_usec);
}

double time_ratio(struct timeval num, struct timeval denom) {
    return (num.tv_sec + (double) num.tv_usec / 1000000) /
        (denom.tv_sec + (double) denom.tv_usec / 1000000);
}

int main(void) {
    unsigned int data_buf_size = MAX_BUFFER_SIZE;
    uchar* data = (uchar*) malloc(data_buf_size);
    if (data == NULL) return -1;

    if (open_bridges() == -1) return -1;

    for (int iteration = 0; iteration < ITERATION_COUNT; iteration++) {
        printf("Iteration %d\n", iteration + 1);

        int data_size = rand_between(MIN_BUFFER_SIZE, MAX_BUFFER_SIZE);
        printf("Working on a %db buffer\n", data_size);

        for (int i = 0; i < data_size; i++) {
            data[i] = (uchar) rand();
        }

        struct timeval sw_begin; gettimeofday(&sw_begin, NULL);
        char* sum = spamsum(data, data_size, 0);
        printf("Spamsum is %s\n", sum);
        struct timeval sw_end; gettimeofday(&sw_end, NULL);
        printf("Software: "); print_time_diff(sw_end, sw_begin);

        struct timeval begin;
        gettimeofday(&begin, NULL);

        // Write data out to accelerator
        write_word(STREAM_LENGTH, data_size);
        write_byte(STREAM_FIRST, data[0]);
        for (int i = 1; i < data_size - 1; i++) {
            write_byte(STREAM_INTER, data[i]);
        }
        write_byte(STREAM_FINAL, data[data_size - 1]);

        struct timeval write_done;
        gettimeofday(&write_done, NULL);

        // Wait for acknowledgement
        int timeout_count = 10000; // up to 10 ms
        while (read_byte(RES_OK) != 1 && timeout_count != 0) {}
        if (timeout_count == 0) printf("Timed out...\n");

        struct timeval ack_seen;
        gettimeofday(&ack_seen, NULL);

        /*printf("res_ok was %u\n", lwb_read_byte(RES_OK));
        printf("BSN: %u\n", lwb_read_byte(RES_BSN));
        printf("fslen: %u\n", lwb_read_byte(RES_FSLEN));
        printf("hslen: %u\n", lwb_read_byte(RES_HSLEN));*/

        struct timeval done;
        gettimeofday(&done, NULL);

        char* accel_result = get_spamsum_string();
        printf("%s\n", accel_result);

        if (strcmp(sum, accel_result) == 0) {
            printf("Results matched\n");
        } else {
            printf("Result mismatch\n");
            if (MAX_BUFFER_SIZE <= 1024) {
                printf("sv = '{");
                for (int i = 0; i < data_size - 1; i++) {
                    printf("%u,", data[i]);
                }
                printf("%u};", data[data_size - 1]);
            }
            getchar();
        }

        printf("Write: "); print_time_diff(write_done, begin);
        printf("Wait:  "); print_time_diff(ack_seen, write_done);
        printf("Done:  "); print_time_diff(done, ack_seen);
        printf("Total: "); print_time_diff(done, begin);

        struct timeval time_sw, time_hw;
        timersub(&sw_end, &sw_begin, &time_sw);
        timersub(&done, &begin, &time_hw);
        printf("Adv: %f\n===============\n", time_ratio(
            time_sw, time_hw
        ));

        free(sum);
        free(accel_result);
    }

    close_bridges();
    free(data);
}
