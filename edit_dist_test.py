# Based off the code of ssdeep
# https://github.com/ssdeep-project/ssdeep/blob/master/edit_dist.c
# Used to both build intuition around the edit distance algorithm, and to generate test data

MAX_LEN = 64

COST_INSERT = 1
COST_DELETE = 1
COST_REPLACE = 2

def edit_dist(hash1, hash2):
    """
    Computes the weighted edit distance between two input strings of maximum length MAX_LEN (= 64)
    """
    t = [
        [0 for _ in range(MAX_LEN + 1)],
        [0 for _ in range(MAX_LEN + 1)]
    ]
    t1 = t[0]
    t2 = t[1]

    for i in range(len(hash2) + 1):
        t1[i] = i * COST_DELETE

    #print("Initial state:\n   {", t1[::-1], "}\n   {", t2[::-1], "}")

    for i in range(len(hash1)):
        t2[0] = (i + 1) * COST_INSERT

        for j in range(len(hash2)):
            cost_a = t1[j + 1] + COST_INSERT
            cost_d = t2[j] + COST_DELETE
            cost_r = t1[j] + (0 if hash1[i] == hash2[j] else COST_REPLACE)
            t2[j + 1] = min(cost_a, cost_d, cost_r)
        
        t1, t2 = t2, t1

        #print("Now:\n   {", t1[::-1], "}\n   {", t2[::-1], "}")
    
    return t1[len(hash2)]

def spamsum_compare(hash1, hash2):
    """
    Computes the similarity score between two hashes, as per spamsum's implementation
    """
    # TODO: make scoring make sense
    # Require at least a 7-character common substring
    if (has_common_substring(hash1, hash2)):
        dist = edit_dist(hash1, hash2)
        # TODO: rescaling
        return dist
    else:
        # Maximum possible edit distance for two strings (= 128)
        return COST_REPLACE * MAX_LEN

ROLLING_WINDOW = 7

def has_common_substring(hash1, hash2):
    """
    Verifies hash1 and hash2 share at least 7 common consecutive characters (one rolling window)
    """
    if (len(hash1) < ROLLING_WINDOW or len(hash2) < ROLLING_WINDOW):
        return False

    hash1_parts = { hash1[i:i+ROLLING_WINDOW] for i in range(len(hash1) - ROLLING_WINDOW + 1) }
    hash2_parts = { hash2[i:i+ROLLING_WINDOW] for i in range(len(hash2) - ROLLING_WINDOW + 1) }

    return len(hash1_parts.intersection(hash2_parts)) > 0

def eliminate_runs(hash):
    """
    Truncate all runs of 3 or more repeated characters
    """
    last_char = ""
    seen = 0
    output = ""

    for char in hash:
        if (last_char == char):
            if (seen < 3):
                print(f"Saw {char}, repeated instance number {seen}")
                output += char
            else:
                print(f"Eliminated repeated char {char}")
            seen += 1
        else:
            print("Different to previous")
            last_char = char
            seen = 0
            output += char

    return output

def hash_to_hex(hash):
    """
    Given a hash base64 string, convert it to an octal literal in Verilog format
    """
    # This has to be DIYd as spamsum works on 6-bit units as base64 chars, whereas the Verilog
    # implementation uses the minimum 6 bits to represent each base64 char the software version uses
    # Base64 decoding would fail as it assumes the string represents byte-aligned data instead.
    output = ""
    for char in hash:
        value = -1
        if ("A" <= char and char <= "Z"):
            value = ord(char) - ord("A")
        elif ("a" <= char and char <= "z"):
            value = ord(char) - ord("a") + 26
        elif ("0" <= char and char <= "9"):
            value = ord(char) - ord("0") + 52
        elif (char == "+"):
            value = 62
        elif (char == "/"):
            value = 63
        else:
            raise Exception("Invalid base64 character")
        print(char, "-->", value, "-->", f"{value:0=2o}")
        output += f"{value:0=2o}"
    return f"{len(output)}'o{output}"

def print_2d(arr):
    for row in arr:
        print(row)
    print()

from collections import deque

VERBOSE = False

LEN = 4
def edit_dist_2d(s1, s2):
    """
    Alternate edit distance algorithm optimised for FPGA implementation
    """
    t = [["x" for _ in range(LEN+1)] for _ in range(LEN+1)]
    prev_t = [["x" for _ in range(LEN+1)] for _ in range(LEN+1)]
    # Stores intermediate solutions from diagonals
    res_array = deque(["x" for _ in range(2*LEN+1)])

    h1 = s1.ljust(LEN)
    h2 = s2.ljust(LEN)

    # a is the "diagonal number", representing which diagonal to work on
    for a in range(2*LEN + 1):
        res_array.pop()
        res_array.appendleft('x')
        if (VERBOSE):
            print_2d(t)
            print("res:", res_array)
        # Find the cell coordinates we can work on from this diagonal
        for i in range(LEN+1):
            j = a - i
            # Skip out-of-bounds cells
            if (i < 0 or j < 0 or i > LEN or j > LEN):
                continue
            # Compute edit distance
            if (VERBOSE): print(f"a={a} Working on [{i}][{j}]")
            r = 0
            if (i == 0 or j == 0):
                if (VERBOSE): print("  Is an edge cell")
                if (i == len(s1) and j == len(s2)):
                    # Solution cell
                    if (VERBOSE): print("  Is a solution cell!")
                    res_array[a] = max(i, j)
                else:
                    if (VERBOSE): print("  Is a constant value")
                    pass
                t[i][j] = max(i, j)
            else:
                if (VERBOSE): print("  Is a centre cell")
                if (i == len(s1) and j == len(s2)):
                    if (VERBOSE): print("  Is a solution cell!")
                    # Solution cell
                    res_array[a] = min(
                        t[i-1][j] + COST_INSERT,
                        t[i][j-1] + COST_DELETE,
                        prev_t[i-1][j-1] + (0 if h1[i-1] == h2[j-1] else COST_REPLACE)
                    )
                else:
                    if (VERBOSE): print("  Is an edit distance value")
                    pass
                t[i][j] = min(
                    t[i-1][j] + COST_INSERT,
                    t[i][j-1] + COST_DELETE,
                    prev_t[i-1][j-1] + (0 if h1[i-1] == h2[j-1] else COST_REPLACE)
                )
        for x in range(LEN+1):
            for y in range(LEN+1):
                prev_t[x][y] = t[x][y]
    return res_array[-1]

import itertools
def test_strs_helper():
    alphabet = "ABCD"
    assert(len(alphabet) == LEN)
    yield map(''.join, itertools.product(alphabet, repeat=1))
    yield map(''.join, itertools.product(alphabet, repeat=2))
    yield map(''.join, itertools.product(alphabet, repeat=3))
    yield map(''.join, itertools.product(alphabet, repeat=4))
def test_strs():
    yield ""
    for m in test_strs_helper():
        for x in m:
            yield x

def verify_edit_dist_2d():
    for s1 in test_strs():
        for s2 in test_strs():
            if (edit_dist(s1, s2) != edit_dist_2d(s1, s2)):
                print(f"Failed for {s1}/{s2}")
                print("Got", edit_dist_2d(s1, s2), "wanted", edit_dist(s1, s2))
                input()
    print("Done")
