/*
 *  Module that provides an Avalon-MM interface to a hash_comparator module
 *
 *  Spamsum port (write-only):
 *    0x00: spamsum 1 block size number
 *    0x01: spamsum 1 full-size digest length
 *    0x02: spamsum 1 half-size digest length
 *    0x04-43: spamsum 1 full-size digest data
 *    0x44-63: spamsum 1 half-size digest data
 *    0x80: spamsum 2 block size number
 *    0x81: spamsum 2 full-size digest length
 *    0x82: spamsum 2 half-size digest length
 *    0x84-C3: spamsum 2 full-size digest data
 *    0xC4-E3: spamsum 2 half-size digest data
 *
 *  Notes:
 *  - Values are preserved after dispatching a transaction
 *  - Writing to unused areas of the address map is fine.
 *  - Quartus-related weirdness requires that all unused parts of digests are padded with 0s.
 *    Failure to do so is likely to produce edit distance results of 128 for identical digests.
 *
 *  Status port (read-only):
 *    0x00-FF: result status registers for corresponding transaction ID
 *    [6:0]: edit distance result
 *    [30]:  flag set if results have incompatible block sizes
 *    [31]:  flag indicating results available
 *
 *  Control port (write-only):
 *    0x00: initiate calculation, using written value as transaction ID
 */

import common::*;
import edit_dist_pipeline_common::*;
module avalon_hash_comparator (
    input  logic        clk,
    input  logic        reset,

    // Spamsum port
    input  logic  [7:0] spamsum_address,
    input  logic        spamsum_write,
    input  t_byte       spamsum_writedata,

    // Status port
    input  t_byte       result_address,
    output integer      result_readdata,

    // Control port
    input  logic        control_write,
    input  t_byte       control_writedata,
    output logic        control_waitrequest
);

t_var_hash hash1, hash2;
t_txn_id txn_id_in;
logic do_request;
logic ready;
t_edit_dist edit_dist;
t_txn_id txn_id_out;
logic output_valid;

t_spamsum sum1, sum2;

struct {
    logic is_done;
    logic is_incomparable;
    t_edit_dist edit_dist;
} result_data [2**TxnBits];

assign result_readdata = result_data[result_address].is_done << 31
                       | result_data[result_address].is_incomparable << 30
                       | result_data[result_address].edit_dist;

hash_comparator hc (
    .clk(clk),
    .rst(reset),
    .hash1_in(hash1),
    .hash2_in(hash2),
    .txn_id_in(txn_id_in),
    .input_valid(do_request),
    .ready(ready),
    .edit_dist(edit_dist),
    .txn_id_out(txn_id_out),
    .output_valid(output_valid)
);

// Handle control_waitrequest signal
always_comb begin
    // If reset, Avalon-MM spec requires that "To avoid system lockup, a device should assert
    // waitrequest when in reset."
    if (reset)
        control_waitrequest = 1;
    else
        control_waitrequest = !ready;
end

always_ff @(posedge clk or posedge reset) begin
    if (reset) begin
        for (int i = 0; i < 2**TxnBits; i++) begin
            result_data[i].is_done <= 0;
            result_data[i].is_incomparable <= 0;
        end
    end else begin
        txn_id_in <= control_writedata;
        // Handle any results produced
        if (output_valid) begin
            result_data[txn_id_out].edit_dist <= edit_dist;
        end

        // Update "is_done" registers:
        // - Goes to 0 when operation initiated
        // - Goes to 1 when operation completes
        // control_write && ready ==> transaction will be dispatched this cycle
        for (int i = 0; i < 2**TxnBits; i++) begin
            if (control_write && ready && i == control_writedata)
                result_data[i].is_done <= 0;
            else if (output_valid && i == txn_id_out)
                result_data[i].is_done <= 1;
        end

        // Handle block size differences when initiating a write
        if (control_write && ready) begin
            if (sum1.block_size == sum2.block_size) begin
                hash1 <= sum1.hash_full;
                hash2 <= sum2.hash_full;
                do_request <= 1;
                result_data[control_writedata].is_incomparable <= 0;
            end else if (sum1.block_size == sum2.block_size + 1) begin
                hash1 <= sum1.hash_full;
                hash2 <= '{digest: {{32{6'b0}}, sum2.hash_half.digest}, len: sum2.hash_half.len};
                do_request <= 1;
                result_data[control_writedata].is_incomparable <= 0;
            end else if (sum1.block_size + 1 == sum2.block_size) begin
                hash1 <= '{digest: {{32{6'b0}}, sum1.hash_half.digest}, len: sum1.hash_half.len};
                hash2 <= sum2.hash_full;
                do_request <= 1;
                result_data[control_writedata].is_incomparable <= 0;
            end else begin
                do_request <= 0;
                result_data[control_writedata].is_incomparable <= 1;
            end
        end else begin
            do_request <= 0;
        end

        // Handle changes to sum1/2
        if (spamsum_write) begin
            // sum1
            if (spamsum_address == 8'h00)
                sum1.block_size <= spamsum_writedata;
            else if (spamsum_address == 8'h01)
                sum1.hash_full.len <= spamsum_writedata;
            else if (spamsum_address == 8'h02)
                sum1.hash_half.len <= spamsum_writedata;
            else if (spamsum_address >= 8'h04 && spamsum_address <= 8'h43)
                sum1.hash_full.digest[spamsum_address - 8'h04] <= spamsum_writedata;
            else if (spamsum_address >= 8'h44 && spamsum_address <= 8'h63)
                sum1.hash_half.digest[spamsum_address - 8'h44] <= spamsum_writedata;
            // sum2
            else if (spamsum_address == 8'h80)
                sum2.block_size <= spamsum_writedata;
            else if (spamsum_address == 8'h81)
                sum2.hash_full.len <= spamsum_writedata;
            else if (spamsum_address == 8'h82)
                sum2.hash_half.len <= spamsum_writedata;
            else if (spamsum_address >= 8'h84 && spamsum_address <= 8'hC3)
                sum2.hash_full.digest[spamsum_address - 8'h84] <= spamsum_writedata;
            else if (spamsum_address >= 8'hC4 && spamsum_address <= 8'hE3)
                sum2.hash_half.digest[spamsum_address - 8'hC4] <= spamsum_writedata;
        end
    end
end

endmodule
