/*
 *  A test bench module to exercise the hash_comparator module
 */

// Have 10M units/sec for 10MHz clock, and simulate 10x per clock cycle
`timescale 100ns/10ns

import common::*;
import verification_common::*;

module tb_hash_comparator_2d;
    // DUT

    bit clk;
    bit reset;
    t_var_hash h1;
    t_var_hash h2;
    bit input_valid;

    t_var_hash h1_out;
    t_var_hash h2_out;
    t_edit_dist edit_dist;
    logic output_valid;

    hash_comparator_2d hc (
        .clk(clk),
        .reset(reset),
        .hash1_in(h1),
        .hash2_in(h2),
        .input_valid(input_valid),
        .hash1_out(h1_out),
        .hash2_out(h2_out),
        .edit_dist(edit_dist),
        .output_valid(output_valid)
    );

    // Testing code
    int tests_passed = 0;
    int tests_failed = 0;

    int random_tests = 1000;

    // These values should differ by no more than MaxDigestLength + 2
    int forever_requests = 0;
    int forever_validated = 0;

    task simple_test(input string name, s1, s2, bit verbose = 1);
        static int cycles;
        static int expected;
        expected = compute_edit_distance(s1, s2);

        if (verbose) $display("T=%0t [%s] Asserting signals", $time, name);
        cycles <= 0;
        h1 <= b64_string_to_hash(s1);
        h2 <= b64_string_to_hash(s2);
        input_valid <= 1;
        @(posedge clk);
        assert(compute_edit_distance(var_hash_to_string(h1), var_hash_to_string(h2)) == expected);

        h1 <= '{default: 'x};
        h2 <= '{default: 'x};
        input_valid <= 0;

        while (!output_valid) begin
            cycles = cycles + 1;
            @(posedge clk);
        end

        if (verbose) $write("T=%0t [%s] Output after %0d cycles was ", $time, name, cycles);
        if (edit_dist == expected) begin
            if (verbose) $display("OK");
            tests_passed++;
        end else begin
            if (verbose)
                $error("not OK!\n --== Output was %0d, expected %0d ==--", edit_dist, expected);
            else
                $error("T=%0t [%s] Output was %0d, expected %0d", $time, name, edit_dist, expected);
            $error("Inputs were %s vs %s", s1, s2);
            tests_failed++;
            $stop();
        end

        // Check the var_hash_to_string and validate_result functions
        validate_result(h1_out, h2_out, edit_dist);
    endtask

    task run_tests();
        // Short hashes with simple transformations
        simple_test("A|A", "A", "A");
        simple_test("A|b", "A", "b");
        simple_test("b|A", "b", "A");
        simple_test("A|aaa", "A", "aaa");
        simple_test("Aaa|A", "Aaa", "A");

        // Two completely non-alike hashes of length x should be different by 2x
        simple_test("AAA|BBB", "AAA", "BBB");

        // Long hashes should work correctly
        simple_test(
            "max*A|max*B",
            {MaxDigestLength{"A"}},
            {MaxDigestLength{"B"}}
        );

        // Identical hashes should match; check parsing of last two b64 symbols
        simple_test("==", "+/a", "+/a");

        // Inserted one character ==> 1
        simple_test("+1", "AxB", "AxxB");
        // Removed one character ==> 1
        simple_test("-1", "AxB", "AB");
        // Replaced one character ==> 2
        simple_test("~~", "AxB", "AyB");

        // Perform random testing
        for (int i = 0; i < random_tests; i++)
            simple_test($sformatf("Rand %0d", i), get_random_hash_string(), get_random_hash_string());

        if (tests_failed > 0)
            $display("FAILED %0d tests of %0d", tests_failed, tests_failed + tests_passed);
        else
            $display("PASSED after %0d tests", tests_passed);
        $stop(0);

        // Perform endless random testing
        forever begin
            // Set inputs
            h1 <= b64_string_to_hash(get_random_hash_string(1));
            h2 <= b64_string_to_hash(get_random_hash_string(1));
            input_valid <= 1;
            forever_requests++;

            // Validate results
            if (output_valid) begin
                validate_result(h1_out, h2_out, edit_dist);
                forever_validated++;
            end

            // Check we've not lost requests in the pipeline
            if ((forever_requests - forever_validated) > MaxDigestLength + 2) begin
                $display("Out of sync! Requested %0d, validated %d", forever_requests, forever_validated);
                $stop();
            end

            @(posedge clk);
        end
    endtask;

    always #0.5 clk = ~clk;
    initial begin
        clk = 1;
        reset = 1;
        @(posedge clk);
        reset = 0;
        @(posedge clk);
        run_tests();
    end

    // Utility functions

    // Generate a random hash string of length in [1, MaxDigestLength]
    // When mode = 1, use length in range [(MaxDigestLength/2)+1, MaxDigestLength] to better
    // resemble real spamsums
    function automatic string get_random_hash_string(bit mode = 0);
        int len = $urandom_range(mode ? (MaxDigestLength/2)+1 : 1, MaxDigestLength);
        string result = "";
        for (int i = 0; i < len; i++)
            result = {result, B64Alphabet[$urandom_range(0, B64Alphabet.len() - 1)]};
        return result;
    endfunction

    // Compute the edit distance of a and b
    function automatic int compute_edit_distance(input string a, b);
        int t [1:0][MaxDigestLength:0] = '{default: '{default: 0} };

        bit t1 = 0;
        bit t2 = 1;

        int temp_cost_a = 0;
        int temp_cost_d = 0;
        int temp_cost_r = 0;

        // Fill out first row
        for (int i = 0; i < b.len() + 1; i++)
            t[t1][i] = i * DeleteCost;

        // Fill out next row and swap until cost matrix filled out
        for (int i = 0; i < a.len(); i++) begin
            t[t2][0] = (i + 1) * InsertCost;

            for (int j = 0; j < b.len(); j++) begin
                temp_cost_a = t[t1][j + 1] + InsertCost;
                temp_cost_d = t[t2][j] + DeleteCost;
                temp_cost_r = t[t1][j] + (a[i] == b[j] ? 0 : ReplaceCost);
                t[t2][j + 1] = min3(temp_cost_a, temp_cost_d, temp_cost_r);
            end

            t1 = ~t1; t2 = ~t2;
        end

        // Extract and return final edit distance
        return t[t1][b.len()];
    endfunction

    // Validate a set of (hash1, hash2, edit_dist) values
    function automatic void validate_result(input t_var_hash h1, h2, input t_edit_dist edit_dist);
        string s1 = var_hash_to_string(h1);
        string s2 = var_hash_to_string(h2);
        int expected = compute_edit_distance(s1, s2);
        if (edit_dist == expected) begin
            tests_passed++;
        end else begin
            $error("T=%0t Output was %0d, expected %0d", $time, edit_dist, expected);
            $error("Inputs were %s vs %s", s1, s2);
            tests_failed++;
            $stop();
        end
    endfunction

endmodule
