/*
 *  A module providing a pipelined implementation of one step of the edit distance algorithm.
 *  
 *  Inputs:
 *  - in, out: pipeline input and results
 */

import common::*;
import edit_dist_pipeline_common::*;

module edit_dist_pipeline (
    input  logic            clk,
    input  logic            rst,
    
    input  t_pipeline_state in,
    output t_pipeline_state out,
    output t_pipeline_state penultimate
);
    t_pipeline_state pipeline [PipelineStages-1:0];

    assign out = final_processing(pipeline[PipelineStages-1]);
    assign penultimate = pipeline[PipelineStages-2];
    
    always_ff @(posedge clk or posedge rst) begin
        if (rst) begin
            // Set each pipeline entry to bubble type, everything else don't cares
            for (int i = 0; i < PipelineStages; i++)
                pipeline[i] <= '{
                    e_type: BUBBLE,
                    default: 'x
                };
        end else begin
            // First stage of pipeline is taken from input
            pipeline[0] <= in;
            // Subsequent stages are derived from partial solve of previous steps, working GroupSize
            // base64_chars at a time
            for (int i = 0; i < PipelineStages - 1; i++) begin
                pipeline[i + 1] <= solve_inter_step(pipeline[i], i * GroupSize,
                    min2((i+1) * GroupSize - 1, MaxDigestLength));
            end
        end
    end
    
    // Given a pipeline state, compute the next states for its cells in [from, to]
    function t_pipeline_state solve_inter_step(input t_pipeline_state prev,
        input t_dist_array_idx from, to);
        t_pipeline_state result;
        // Copy everything across
        result = prev;
        // If active, manipulate dist arrays
        if (result.e_type == ACTIVE) begin
            for (int i = from; i <= to; i++) begin
                if (i == 0)
                    result.active.dist_array[1][i] = (result.active.hash1_index + 1) * InsertCost;
                else
                    result.active.dist_array[1][i] = min3(
                        /* cost_a */ result.active.dist_array[0][i] + InsertCost,
                        /* cost_d */ result.active.dist_array[1][i - 1] + DeleteCost,
                        /* cost_r */ result.active.dist_array[0][i - 1] + (
                            (result.hash1.digest[result.active.hash1_index]
                            == result.hash2.digest[i - 1]) ? 0 : ReplaceCost
                        )
                    );
            end
        end
        return result;
    endfunction
    
    // Return a copy of a dist_array with its rows swapped
    function t_dist_array swap_dist_arrays(input t_dist_array state);
        t_dist_array result;
        // Swap dist arrays
        result[0] = state[1];
        result[1] = state[0];
        return result;
    endfunction
    
    // Do any final activities required before outputting a state
    // ACTIVE: swap the dist arrays and increment hash1_index
    // others: do nothing
    function t_pipeline_state final_processing(input t_pipeline_state state);
        t_pipeline_state result;
        result = state;
        // Increment hash1_index and swap dist array rows if required
        if (state.e_type == ACTIVE) begin
            result.active.hash1_index++;
            result.active.dist_array = swap_dist_arrays(result.active.dist_array);
        end
        return result;
    endfunction

endmodule
