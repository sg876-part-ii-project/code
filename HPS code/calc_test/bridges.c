#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#define H2F_HWB_BASE (0xC0000000)
#define H2F_HWB_SPAN (0x3BFFFFFF)
#define H2F_LWB_BASE (0xFF200000)
#define H2F_LWB_SPAN (0x001FFFFF)

void* h2f_lwb;
void* h2f_hwb;

int open_physical(int* fd) {
	if (*fd == -1) { // fd not already open
		// Try and open /dev/mem
		if ((*fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
			printf("Error: couldn't open /dev/mem\n");
			return -1;
		}
	}
	return 0;
}

void* map_physical(int fd, unsigned int base, unsigned int span) {
	void* virtual_base = mmap(NULL, span, (PROT_READ | PROT_WRITE),
		MAP_SHARED, fd, base);
	if (virtual_base == MAP_FAILED) {
		printf("Error: mmap failed\n");
		close(fd);
		return NULL;
	} else {
		return virtual_base;
	}
}

int unmap_physical(void* virtual_base, unsigned int span) {
	if (munmap(virtual_base, span) != 0) {
		printf("Error: can't munmap\n");
		return -1;
	}
	return 0;
}

int phys_fd;

// Initialise the bridge FDs and pointers
int open_bridges(void) {
	phys_fd = -1;
	if ((open_physical(&phys_fd)) == -1) return -1;
	if (!(h2f_lwb = map_physical(phys_fd, H2F_LWB_BASE, H2F_LWB_SPAN)))
		return -1;
	if (!(h2f_hwb = map_physical(phys_fd, H2F_HWB_BASE, H2F_HWB_SPAN)))
		return -1;
	return 0;
}

void close_bridges(void) {
	unmap_physical(h2f_lwb, H2F_LWB_SPAN);
	unmap_physical(h2f_hwb, H2F_HWB_SPAN);
	close(phys_fd);
}
