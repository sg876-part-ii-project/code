vlog common.sv
vlog verification_common.sv
vlog edit_dist_pipeline_common.sv
vlog edit_dist_pipeline.sv
vlog hash_comparator.sv
vlog avalon_hash_comparator.sv
vlog tb_a_comp.sv

vsim work.tb_a_comp

add wave *
add wave /tb_a_comp/ahc/*
