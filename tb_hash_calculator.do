vlib work

vlog common.sv
vlog verification_common.sv
vlog hash_state.sv
vlog hash_calculator.sv
vlog tb_hash_calculator.sv

vsim work.tb_hash_calculator

add wave -unsigned tb_hash_calculator/*
add wave -unsigned tb_hash_calculator/hc/*
