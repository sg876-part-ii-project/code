`timescale 100ns/10ns

import common::*;
module tb_a_calc;

    logic clk;
    logic reset;

    logic  [1:0] stream_address;
    logic        stream_write;
    t_byte       stream_writedata;

    logic  [1:0] length_address;
    logic        length_write;
    t_byte       length_writedata;

    logic  [6:0] result_address;
    logic        result_read;
    t_byte       result_readdata;

    avalon_hash_calculator ahc (.*);

    always #0.5 clk = ~clk;
    initial begin
        clk = 1;
        reset = 1;
        stream_write = 0;
        length_write = 0;
        result_read = 0;
        @(posedge clk);
        reset = 0;
        @(posedge clk);
        length_address = 0;
        length_write = 1;
        length_writedata = 8;
        @(posedge clk);
        length_address = 1;
        length_writedata = 0;
        @(posedge clk);
        length_address = 2;
        length_writedata = 0;
        @(posedge clk);
        length_address = 3;
        length_writedata = 0;
        @(posedge clk);
        length_address = 'x;
        length_write = 0;
        length_writedata = 'x;

        stream_address = 0;
        stream_write = 1;
        stream_writedata = 8'h00;
        @(posedge clk);
        stream_address = 1;
        stream_writedata = 8'h12;
        @(posedge clk);
        stream_writedata = 8'h23;
        @(posedge clk);
        stream_writedata = 8'h55;
        @(posedge clk);
        stream_writedata = 8'h23;
        @(posedge clk);
        stream_writedata = 8'h32;
        @(posedge clk);
        stream_writedata = 8'h64;
        @(posedge clk);
        stream_address = 2;
        stream_writedata = 8'hFF;
        @(posedge clk);
        stream_address = 'x;
        stream_write = 0;
        stream_writedata = 'x;
        @(posedge clk);
        result_address = 0;
        result_read = 1;
        @(posedge clk);
        @(posedge clk);
        while (result_readdata != 1) begin
            $display("Wait for result ready");
            @(posedge clk);
        end
        $display("Ready");

        result_address = 'h08;
        @(posedge clk);

        result_address = 'h09;
        @(posedge clk);

        result_address = 'h0A;
        @(posedge clk);

        result_address = 'h10;
        @(posedge clk);
        result_address = 'h11;
        @(posedge clk);
        result_address = 'h50;
        @(posedge clk);
        result_address = 'h51;
        @(posedge clk);

        $stop();
    end

endmodule
