/*
 *  Module that provides an Avalon-MM interface to the hash calculator, spread across three ports
 *
 *  Stream port address map:
 *    0x0: write first byte of stream here
 *    0x1: write intermediate bytes of stream here
 *    0x2: write final byte of stream here
 *
 *  Length port address map:
 *    0x0-3 (4 bytes): write stream length in bytes
 *
 *  Result port address map:
 *    0x00: is OK? (Reads 1 when result ready, 0 otherwise)
 *    0x08: block size number
 *    0x09: full-length digest length
 *    0x0A: half-length digest length
 *    0x10-4F (64 bytes): full-length hash data (unpacked: only lower 6 bits of byte set)
 *    0x50-6F (32 bytes): half-length hash data (also unpacked)
 *
 *  Procedure: write length to length port, write data stream to stream port, and
 *  read result from result port after final byte.
 */

import common::*;
module avalon_hash_calculator (
    input  logic        clk,
    input  logic        reset,

    // Stream port
    input  logic  [1:0] stream_address,
    input  logic        stream_write,
    input  t_byte       stream_writedata,

    // Length port
    input  logic  [1:0] length_address,
    input  logic        length_write,
    input  t_byte       length_writedata,

    // Result port
    input  logic  [6:0] result_address,
    input  logic        result_read,
    output t_byte       result_readdata
);

t_byte byte_in;
logic byte_valid, stream_start, stream_end;
//t_stream_len stream_length; // 38 bits - must increase length_address width
integer unsigned stream_length; // 32 bits

t_spamsum spamsum;
logic spamsum_valid, module_ready;

hash_calculator hc (
    .clk(clk),
    .reset(reset),
    .input_byte_in(byte_in),
    .input_byte_valid(byte_valid),
    .input_stream_start(stream_start),
    .input_stream_end(stream_end),
    .input_stream_length(38'(stream_length)),
    .spamsum(spamsum),
    .spamsum_valid(spamsum_valid),
    .ready(module_ready)
);

t_spamsum result;
logic result_latched;

always_ff @(posedge clk or posedge reset)
    if (reset) begin
        result          <= '{default: 'x};
        result_latched  <= 0;

        byte_in         <= 'x;
        byte_valid      <= 0;
        stream_start    <= 'x;
        stream_end      <= 'x;
        stream_length   <= 0;

        result_readdata <= 'x;
    end else begin
        if (stream_write && stream_start)
            // Invalidate any result held on stream start
            result_latched <= 0;
        else if (spamsum_valid) begin
            // Latch any results produced
            result <= spamsum;
            result_latched <= 1;
        end

        if (stream_write) begin
            byte_in      <= stream_writedata;
            byte_valid   <= 1;
            stream_start <= (stream_address == 2'h0);
            stream_end   <= (stream_address == 2'h2);
        end else begin
            byte_valid <= 0;
        end

        if (length_write) begin
            //if (length_address >= 3'h0 && length_address < 3'h4)
                stream_length[(length_address * 8) +: 8] <= length_writedata;
            //else if (length_address == 3'h4)
            //    stream_length[37:32] <= length_writedata[5:0];
        end

        if (result_read) begin
            if (result_address == 'h00)
                result_readdata <= 8'(result_latched);
            else if (result_address == 'h08)
                result_readdata <= 8'(result.block_size);
            else if (result_address == 'h09)
                result_readdata <= 8'(result.hash_full.len);
            else if (result_address == 'h0A)
                result_readdata <= 8'(result.hash_half.len);
            else if (result_address >= 'h10 && result_address <= 'h4F)
                result_readdata <= result.hash_full.digest[
                    result_address - 'h10
                ];
            else if (result_address >= 'h50 && result_address <= 'h6F)
                result_readdata <= result.hash_half.digest[
                    result_address - 'h50
                ];
            else
                result_readdata <= 'x;
        end
    end

endmodule
