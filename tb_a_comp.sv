`timescale 100ns/10ns

import common::*;
import edit_dist_pipeline_common::t_txn_id;
import verification_common::*;
module tb_a_comp;

    logic clk;
    logic reset;
    
    // Spamsum port
    logic  [7:0] spamsum_address;
    logic        spamsum_write;
    t_byte       spamsum_writedata;

    // Status port
    t_txn_id     result_address;
    integer      result_readdata;

    // Control port
    logic        control_write;
    t_byte       control_writedata;
    logic        control_waitrequest;

    avalon_hash_comparator ahc (.*);

    always #0.5 clk = !clk;
    initial begin
        clk = 1;
        reset = 1;
        spamsum_write = 0;
        result_address = 'hBE;
        control_write = 0;

        @(posedge clk);
        reset = 0;

        // 6:AAA:++ vs 3:+++:AAA, expecting 0x80000000

        @(posedge clk);
        write_sum('h00, 1, "AAA", "++");
        write_sum('h80, 0, "+++", "AAA");

        control_write = 1;
        control_writedata = 'hBE;

        do @(posedge clk); while (control_waitrequest == 1);
        control_write = 0;
        control_writedata = 'x;
    end

    task write_fs_digest(input int offset, input string digest);
        assert(digest.len() < MaxDigestLength);
        for (int i = 0; i < MaxDigestLength; i++) begin
            spamsum_write = 1;
            spamsum_address = offset + i;
            spamsum_writedata = (i < digest.len()) ?
                b64_char_to_int(digest[i]) : 'h00;
            @(posedge clk);
        end
        spamsum_write = 0;
        spamsum_address = 'x;
        spamsum_writedata = 'x;
    endtask

    task write_hs_digest(input int offset, input string digest);
        assert(digest.len() < MaxDigestLength/2);
        for (int i = 0; i < MaxDigestLength/2; i++) begin
            spamsum_write = 1;
            spamsum_address = offset + i;
            spamsum_writedata = (i < digest.len()) ?
                b64_char_to_int(digest[i]) : 'h00;
            @(posedge clk);
        end
        spamsum_write = 0;
        spamsum_address = 'x;
        spamsum_writedata = 'x;
    endtask

    task write_sum(input int offset, input byte bsn, input string fs_dig, hs_dig);
        assert(fs_dig.len() < MaxDigestLength);
        assert(hs_dig.len() < MaxDigestLength/2);
        spamsum_write = 1;
        spamsum_address = offset + 'h00;
        spamsum_writedata = bsn;
        @(posedge clk);
        spamsum_address = offset + 'h01;
        spamsum_writedata = fs_dig.len() - 1;
        @(posedge clk);
        spamsum_address = offset + 'h02;
        spamsum_writedata = hs_dig.len() - 1;
        @(posedge clk);
        write_fs_digest(offset + 'h04, fs_dig);
        write_hs_digest(offset + 'h44, hs_dig);
    endtask

endmodule
