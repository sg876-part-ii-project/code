import common::*;

package verification_common;
    string B64Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    typedef struct {
        int unsigned block_size;
        string sig1;
        string sig2;
    } t_tb_spamsum;

    // Convert a string in base64 to a t_var_hash
    function automatic common::t_var_hash b64_string_to_hash(input string str);
        // String must be at least 1 character long
        assert(str.len() > 0);
        // String must not exceed expected length
        assert(str.len() <= common::MaxDigestLength);
        b64_string_to_hash.len = str.len() - 1;

        foreach(b64_string_to_hash.digest[i])
            // Determine correct base64_char to place in digest field, setting x if not used
            b64_string_to_hash.digest[i] = i < str.len() ? b64_char_to_int(str.getc(i)) : 6'oxx;
    endfunction

    // Given input char in base64 format, returns a b64_char for it
    function automatic common::t_b64_char b64_char_to_int(input byte char);
        if (65 <= char && char <= 90)       // char in [A-Z]
            return char - 65;
        else if (97 <= char && char <= 122) // char in [a-z]
            return char - 71;
        else if (48 <= char && char <= 57)  // char in [0-9]
            return char + 4;
        else if (char == 43)                // char is +
            return 62;
        else if (char == 47)                // char is /
            return 63;
        else                                // char not valid base64
            assert(0);
            return 0;
    endfunction

    // Convert a var_hash to a base64 string
    function automatic string var_hash_to_string(input common::t_var_hash h);
        string result;
        // Remember var_hash length field is offset by one
        for (int i = 0; i < h.len + 1; i++)
            result = {result, B64Alphabet[h.digest[i]]};
        return result;
    endfunction
    // As above for a half_var_hash
    function automatic string half_var_hash_to_string(input common::t_half_var_hash h);
        string result;
        // Remember var_hash length field is offset by one
        for (int i = 0; i < h.len + 1; i++)
            result = {result, B64Alphabet[h.digest[i]]};
        return result;
    endfunction
endpackage
