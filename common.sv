/*
 *  Contains shared functions and data type definitions
 */

package common;
    // Helpers: return the minimum of the two or three arguments given
    function integer min2(input integer a, b);
        return (a < b) ? a : b;
    endfunction

    function integer min3(input integer a, b, c);
        return min2(min2(a, b), c);
    endfunction

    function integer max2(input integer a, b);
        return (a > b) ? a : b;
    endfunction

    function integer max3(input integer a, b, c);
        return max2(max2(a, b), c);
    endfunction

    // Clamp the input value within the (inclusive) range specified
    function integer clamp(input integer value, low_limit, high_limit);
        if (value > high_limit)
            return high_limit;
        else if (value < low_limit)
            return low_limit;
        else
            return value;
    endfunction

    // Maximum length of a hash
    parameter MaxDigestLength = 64;
    // Maximum edit distance score that can be produced
    parameter MaxScore = MaxDigestLength * max2(InsertCost + DeleteCost, ReplaceCost);

    // Costs applied to edit distance calculation
    parameter InsertCost = 1;
    parameter DeleteCost = 1;
    parameter ReplaceCost = 2;

    // Rolling hash parameters
    parameter WindowSize = 7;
    parameter H3Shift = 5;

    // FNV hash parameters
    parameter FNVOffsetBasis = 32'h28021967;
    parameter FNVPrime = 32'h01000193;

    // Note: minimum block size is not configurable directly, due to the cost of modulo operations

    // Maximum block size. Affects the maximum possible stream length.
    // Stream length < 192 GiB when 30. Larger values require logic changes.
    parameter MaxBlockSize = 30;

    // Typedefs
    // 4-state byte type
    typedef logic [7:0] t_byte;
    // Edit distance type
    typedef logic [$clog2(MaxScore):0] t_edit_dist;
    // Base64 character type
    typedef logic [5:0] t_b64_char;

    // Digest type
    typedef t_b64_char [MaxDigestLength-1:0] t_digest;
    // Hash length value type (number of valid t_b64_chars in a t_digest)
    typedef logic [$clog2(MaxDigestLength)-1:0] t_hash_len;
    // Variable-length hash type
    typedef struct {
        t_digest digest;
        t_hash_len len;
    } t_var_hash;
    // Half-length equivalents of t_digest, t_hash_len, t_var_hash
    typedef t_b64_char [(MaxDigestLength/2)-1:0] t_half_digest;
    typedef logic [$clog2(MaxDigestLength/2)-1:0] t_half_hash_len;
    typedef struct {
        t_half_digest digest;
        t_half_hash_len len;
    } t_half_var_hash;

    // Block size number type: actual block size is 3 * 2^{this value}
    typedef logic [$clog2(MaxBlockSize):0] t_block_size_number;
    // Byte stream length type: maximum supported stream length given MaxBlockSize
    // Automatic derivation of this type's width like thisis prevented by 32-bit limit to arithmetic
    //typedef logic [$clog2(MaxDigestLength * 3 * (2 ** MaxBlockSize))-1:0] t_stream_len;
    typedef logic [($clog2(MaxDigestLength * 3) + MaxBlockSize)-1:0] t_stream_len;
    // Spamsum signature type
    typedef struct {
        t_block_size_number block_size;
        t_var_hash hash_full;
        t_half_var_hash hash_half;
    } t_spamsum;

    function automatic t_var_hash initial_vh();
        return '{
            digest: '{default: 'x},
            len: 0
        };
    endfunction

    function automatic t_half_var_hash initial_hvh();
        return '{
            digest: '{default: 'x},
            len: 0
        };
    endfunction

    // Hash comparison function
    // Under testing: verifies unused bits are x
    function logic hash_eq(input t_var_hash a, b);
        logic [MaxDigestLength-1:0] ok_bits;
        `ifdef MODEL_TECH
            static logic a_ok = 1; static bit b_ok = 1;
            // Check no unused base64_chars in digest portion
            foreach (a.digest[i]) begin
                a_ok = a_ok && (
                    i <= a.len ?                // if i corresponds to a b64_char in use
                        defined(a.digest[i]) :  // then require it's a defined value
                        !defined(a.digest[i])   // else require it's undefined
                );
            end
            //assert(a_ok); // removed due to simulation/synthesis mismatches

            foreach (b.digest[i])
                b_ok = b_ok && (
                    i <= b.len ?                // if i corresponds to a b64_char in use
                        defined(b.digest[i]) :  // then require it's a defined value
                        !defined(b.digest[i])   // else require it's undefined
                );
            //assert(b_ok); // removed due to simulation/synthesis mismatches

        `endif
            // Only compare parts of var_hashes that are in range
            for (int i = 0; i < MaxDigestLength; i++)
                ok_bits[i] = (a.digest[i] == b.digest[i]) || i >= (a.len + 1);
            return (a.len == b.len) && (&ok_bits);
    endfunction

    `ifdef MODEL_TECH
        // Tests if the given integer input is defined or not
        function bit defined(input integer a);
            // If a contains at least one x bit, the expression evaluates to x, and is coerced to a 0.
            // If a is defined, either a == 0 or a != 0 will hold, so 1 is returned.
            return (a == 0) || (a != 0);
        endfunction
    `endif

endpackage

