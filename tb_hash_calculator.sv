/*
 * Testbench for the hash_calculator module
 */
// Have 10M units/sec for 10MHz clock, and simulate 10x per clock cycle
`timescale 100ns/10ns

import common::*;
import verification_common::*;

module tb_hash_calculator;
    // DUT

    bit clk = 1;
    bit reset = 1;

    logic [7:0] byte_in;
    logic byte_valid;
    logic stream_start;
    logic stream_end;
    t_stream_len stream_length;

    t_spamsum spamsum_out;
    logic spamsum_valid;
    logic hc_ready;

    // Instantiate the DUT
    hash_calculator hc (
        .clk(clk),
        .reset(reset),
        .input_byte_in(byte_in),
        .input_byte_valid(byte_valid),
        .input_stream_start(stream_start),
        .input_stream_end(stream_end),
        .input_stream_length(stream_length),
        .spamsum(spamsum_out),
        .spamsum_valid(spamsum_valid),
        .ready(hc_ready)
    );

    // Clock
    always #0.5 clk = ~clk;

    // Testing code
    // Test params
    parameter MinStreamLength = 2;
    parameter MaxStreamLength = 4096;

    // Stores expected results
    typedef struct {
        t_tb_spamsum spamsum;
        string test_data;
        integer test_len;
    } t_tb_test_item;
    t_tb_test_item expected_results [$];

    // This thread validates results from the module, including that results aren't being lost
    integer unsigned count_in = 0;
    integer unsigned count_out = 0;
    always #1 begin
        bit ok;
        t_tb_test_item test_item;
        t_tb_spamsum expected;
        // Wait for a result valid signal
        if (spamsum_valid) begin
            count_out++;
            assert(count_in - count_out <= 5);
            // Check the module outputs match the head of the expected_results queue
            test_item = expected_results.pop_front();
            expected = test_item.spamsum;
            ok =   expected.sig1 == var_hash_to_string(spamsum_out.hash_full)
                && expected.sig2 == half_var_hash_to_string(spamsum_out.hash_half)
                && expected.block_size == (3 * 2 ** spamsum_out.block_size);
            if (!ok) begin
                // $display("Wrong result when input was %s", test_item.test_data);
                $display("Test data length was %0d", test_item.test_len);
                $display("Expected: %s", spamsum_format(test_item.spamsum));
                $display(
                    "Got: block size = %0d, full=%s, half=%s", spamsum_out.block_size,
                    var_hash_to_string(spamsum_out.hash_full),
                    half_var_hash_to_string(spamsum_out.hash_half));
                $stop();
            end else
                $display("Validated result %0d", count_out);
                $display("Expected: %s", spamsum_format(test_item.spamsum));
                $display("Got:      %0d:%s:%s", 3 * (1 << spamsum_out.block_size),
                    var_hash_to_string(spamsum_out.hash_full),
                    half_var_hash_to_string(spamsum_out.hash_half));
        end
    end

    // This thread drives the module with test data
    initial begin
        byte unsigned test_data [];
        byte_valid = 0;
        stream_start = 0;
        @(posedge clk);
        reset = 0;

        // Perform random testing
        wait_for_ready();
        forever begin
            test_data = new[$urandom_range(MinStreamLength, MaxStreamLength)];
            foreach (test_data[i]) test_data[i] = $urandom_range(0, 255);
            drive_dut(test_data);
        end
    end

    task automatic drive_dut(input byte unsigned test_data []);
        // Derive the expected output
        t_tb_spamsum expected = spamsum(test_data);
        expected_results.push_back('{
            spamsum: expected,
            test_data: bytes_to_hex(test_data),
            test_len: test_data.size()
        });
        count_in++;
        // Drive the DUT
        for (int i = 0; i < test_data.size(); i++) begin
            byte_in = test_data[i];
            byte_valid = 1;
            stream_start = (i == 0);
            stream_end = (i == test_data.size() - 1);
            stream_length = (i == 0) ? test_data.size() : 'x;
            @(posedge clk);
            do_random_delay(50);
        end
        byte_in = 'x; byte_valid = 0; stream_start = 'x; stream_end = 'x; stream_length = 'x;
        wait_for_ready();
        do_random_delay(5);
    endtask

    // Sometimes waits for a random number of cycles, setting the module inputs to prevent sampling
    // when doing so. Causes a delay one in how_often calls.
    task automatic do_random_delay(input integer how_often);
        if ($urandom_range(0, how_often - 1) == 0) begin
            // Set module inputs to their don't care/don't sample values
            byte_in = 'x;
            byte_valid = 0;
            stream_start = 'x;
            stream_end = 'x;
            stream_length = 'x;
            repeat ($urandom_range(1, 5)) @(posedge clk);
        end
    endtask

    // Wait for the module to signal itself ready
    task automatic wait_for_ready();
        while (!hc_ready) @(posedge clk);
    endtask

    string Hex = "0123456789abcdef";
    function automatic string bytes_to_hex(input byte unsigned in []);
        string result = "{";
        for (int i = 0; i < in.size(); i++) begin
            result = {result, "0x", Hex[(in[i] & 8'hF0) >> 4], Hex[in[i] & 8'h0F]};
            if (i != in.size() - 1) result = {result, ","};
        end
        return {result, "}"};
    endfunction

    // Output a spamsum struct in string format
    function automatic string spamsum_format(input t_tb_spamsum s);
        return $sformatf("%0d:%s:%s", s.block_size, s.sig1, s.sig2);
    endfunction

    // Validation model - transcribed version of the Tridgell, 2002 implementation of spamsum
    typedef struct {
        byte unsigned window[WindowSize-1:0];
        int unsigned h1, h2, h3;
        int unsigned n;
    } t_tb_roll_state;

    function automatic int unsigned roll_hash(input byte unsigned c, ref t_tb_roll_state roll_state);
        roll_state.h2 -= roll_state.h1;
        roll_state.h2 += WindowSize * c;

        roll_state.h1 += c;
        roll_state.h1 -= roll_state.window[roll_state.n % WindowSize];

        roll_state.window[roll_state.n % WindowSize] = c;
        roll_state.n++;

        roll_state.h3 = roll_state.h3 << 5;
        roll_state.h3 ^= c;

        return roll_state.h1 + roll_state.h2 + roll_state.h3;
    endfunction

    function automatic int unsigned roll_reset(ref t_tb_roll_state roll_state);
        roll_state = '{default: 0};
        return 0;
    endfunction

    function automatic int unsigned sum_hash(input byte unsigned c, input integer unsigned h);
        h *= FNVPrime;
        h ^= c;
        return h;
    endfunction

    // Returns a copy of s with all spaces removed
    function automatic string str_strip_spaces(input string s);
        string result = "";
        for (int i = 0; i < s.len(); i++)
            if (s[i] != " ")
                result = {result, s[i]};
        return result;
    endfunction

    // Function that emulates the original spamsum algorithm
    function automatic t_tb_spamsum spamsum(input byte unsigned in []);
        int unsigned length = in.size();
        int unsigned h, h2, h3;
        int unsigned j, n, i, k;
        int unsigned block_size;
        t_tb_spamsum result;
        bit done = 0;
        t_tb_roll_state rs;

        // Guess a first block size
        block_size = 3;
        while (block_size * MaxDigestLength < length)
            block_size *= 2;

        // Mappings from original variables to result:
        // ret  |--> result.sig1
        // ret2 |--> result.sig2

        while (!done) begin
            result.block_size = block_size;
            result.sig1 = {64{" "}}; result.sig2 = {32{" "}};
            j = 0; k = 0;
            h2 = FNVOffsetBasis; h3 = FNVOffsetBasis;
            h = roll_reset(rs);

            for (i = 0; i < length; i++) begin
                h = roll_hash(in[i], rs);
                h2 = sum_hash(in[i], h2);
                h3 = sum_hash(in[i], h3);

                if ((h % block_size) == (block_size - 1)) begin
                    // Have hit a reset point for h2
                    result.sig1[j] = B64Alphabet[h2 % 64];
                    if (j < MaxDigestLength-1) begin
                        h2 = FNVOffsetBasis;
                        j++;
                    end
                end
                if ((h % (block_size * 2)) == ((block_size * 2) - 1)) begin
                    // Have hit a reset point for h3
                    result.sig2[k] = B64Alphabet[h3 % 64];
                    if (k < MaxDigestLength/2 - 1) begin
                        h3 = FNVOffsetBasis;
                        k++;
                    end
                end
            end

            // Put remainder on end of sig
            if (h != 0) begin
                result.sig1[j] = B64Alphabet[h2 % 64];
                result.sig2[k] = B64Alphabet[h3 % 64];
            end

            // Check block size guess
            if (block_size > 3 && j < MaxDigestLength/2) begin
                $display("Halved block size");
                $display("Generated %s", spamsum_format(result));
                block_size = block_size / 2;
            end else
                done = 1;
        end

        result.sig1 = str_strip_spaces(result.sig1);
        result.sig2 = str_strip_spaces(result.sig2);

        return result;
    endfunction
endmodule
