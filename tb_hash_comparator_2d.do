# Script to compile the 2D hash comparator and its test bench, then run the test bench
vlib work

vlog common.sv
vlog verification_common.sv
vlog hash_comparator_2d.sv
vlog tb_hash_comparator_2d.sv

vsim work.tb_hash_comparator_2d

#add wave -unsigned *
add wave -unsigned sim:/tb_hash_comparator_2d/hc/*

