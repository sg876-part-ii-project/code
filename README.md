# Final year project code

This repository contains code for my final year project, relating to edit distance and fuzzy hash algorithms on FPGAs.

## SystemVerilog modules and packages
* `avalon_hash_comparator.sv`, `avalon_hash_calculator.sv`: Avalon-MM interfaces to their respective modules.
* `common.sv`: a package containing shared functions, parameters, datatypes, and functions.
* `edit_distance_pipeline_common.sv`: a package containing edit distance pipeline parameters and datatypes.
* `edit_distance_pipeline.sv`: a module that provides a pipelined implementation of a single step of the edit distance algorithm.
* `hash_calculator.sv`: a module that accepts a stream of bytes and returns a spamsum digest of that data.
* `hash_comparator_2d.sv`: an alternative implementation of the edit distance comparator, suitable only for large to very large FPGAs, or a modified spamsum algorithm.
* `hash_comparator.sv`: a module that accepts pairs of digests and returns their similarity using the Levenshtein edit distance.
* `hash_state.sv`: a module that, given a stream of bytes and a block size, returns a spamsum for that data. Part of the `hash_calculator` module.
* `tb_*.sv`: test benches for their respective modules.
* `verification_common.sv`: a package of verification-specific datatypes, constants, and functions.

## Other code
* `edit_dist_test.py`: a Python scratchpad for edit distance algorithms.
* `tb_*.do`: ModelSim macros that compile their respective module and its dependencies, and invoke their testbench.

# Implementation notes
## The hash format
Within original implementations of spamsum, hashes are represented and worked on as a string of up to 64 base64 characters (see [Andrew Tridgell's implementation, constant SPAMSUM_LENGTH](https://www.samba.org/ftp/unpacked/junkcode/spamsum/spamsum.c)). As each base64 character encodes 6 bits, this gives a maximum of 384 bits of digest to work on.

As hashes can be of variable length, the valid length of hash data is encoded too. To be able to represent digests from 0 to 64 characters long, a 7-bit value would be required; excluding zero-length digests shaves one bit. As this value only occurs for zero-length or all-zero inputs, this should be fine.

Up to 64 `t_base64_chars` as a `t_digest` are combined with a `t_digest_length` value to produce the `t_var_hash` type, providing one variable-length spamsum hash.

Each spamsum consists of a second hash computed at twice the block size, so that inputs near a block size boundary are comparable. This is encoded by a `t_half_var_hash` in the same way as above, although with each datatype adjusted in size accordingly.

A spamsum struct also encodes the block size used to compute the result. This is stored as a `t_block_size_number`; using the knowledge that block sizes are a power-of-two multiple of 3 in the original implementation, we can encode only the power-of-two component. This both saves bits and is a closer match for the process of computing a spamsum digest.

## The edit distance pipeline
I tried three approaches to computing an edit distance. All are derived from the dynamic programming implementation of the Levenshtein distance. (Note "C/s" means "comparisons per second" in the following)

The first was the naive method. This used the row-by-row approach, which means only two rows of the edit distance array have to be stored. It computes the next row from the previous row in a single cycle. This had a very long critical path, as values have to be propagated along the whole row, so achieved an extremely poor Fmax of less than 3 MHz, and throughput in the order of 45-94 kC/s.

The second, and better, approach pipelined the previous method. It uses an internal pipeline of configurable length which, as a set of inputs pass through, computes the next row of the edit distance array. Each set of inputs iterates through the pipeline until it reaches a solution. This means that re-ordering can be introduced between inputs and outputs, but the benefit is great. The highest level of pipelining should allow scaling beyond 120 MHz, with throughput in the range of 1.9-3.8 MC/s.

The third approach computes the full edit distance matrix, working along a pair of array diagonals at a time. By working in groups of two of these diagonals (a "slice"), only one copy of the array has to be kept, as all the values in the slice can be derived from the previous slice or intermediate values within the slice. I expected this to scale poorly, and was surprised to find that it actually requires logic cubic in the input lengths (not just quadratic), which I put down to the increasing range of possible values as the table grows, and the increased logic and interconnect resources required to work with them. In light of the logic estimates provided by Quartus, only the very largest Intel FPGAs might be large enough to use this module. It would have achieved in-order completion, with a throughput exactly equal to one comparison per cycle, and very high achievable clocks (for a reduced implementation, an estimated FMax of around 200 MHz was given). At a rate of 200+ MC/s, this would need memory capable of maintaining a transfer rate around 27 GiB/s to keep the module saturated.

The second approach was chosen, for being the best (configurable!) balance of performance and logic utilisation. While the simple memory-mapped Avalon interface doesn't expose the full performance of the module, a more FPGA-driven approach to acquiring data would make use of that throughput.

## The spamsum algorithm
A broad outline of the spamsum algorithm is given by [Jesse Kornblum](https://doi.org/10.1016/j.diin.2006.06.015). This paper neglects a few implementation details needed for full compatibility with the original spamsum implementation:
* The initial block size is derived differently in spamsum.
* That the full- and half-length outputs are limited to 64 and 32 characters respectively.
    * When spamsum reaches the length limit, the current value of the respective sum hash overwrites the previous final character value, and the sum hash isn't reset. In this way, the final character of the digest is computed over all remaining data
    * After reaching the end of the input, the current state of the respective sum hashes are added to both digests; the same handling of the length limit is applied.
        * This should be conditional on the sum hashes not being 0, which I believe is a way to avoid zero-length inputs producing non-zero-length outputs. I removed that in my implementation for simplicity, as it should only affect one in four billion calculations (or so)!

The only other change I made was to calculate the current position in the window using a wrapping counter, rather than using the number of input bytes modulo 7. This step in the calculation limits performance so the adapted counter should help improve performance, especially given how difficult efficient modulo calculations are on FPGAs. This change means spamsum and its hardware implementation may (probably will) diverge after 4 GiB of input data, as the `n % 7` calculation in the original is discontinuous on overflowing the unsigned 32-bit `n`.

Another trick comes in the determination of reset points. A reset point occurs when the current value of the rolling hash `r` satisfies, for block size `b`, `r % b == b - 1`.

This isn't easy to compute by default on an FGPA, and would have instantiated a DSP block, with considerably worse performance as a result.

This can be improved. First, I used the previous observation that all valid block sizes `b` have a natural number `k` such that `b = 3 * 2^k`.

`r % b == b - 1` can then be decomposed into `(r + 1) % 3 == 0 && (r + 1) % 2^k == 0`.
* The latter is checked using a bitmask generated at runtime.
* The former uses an approach from [Dr. Douglas W. Jones](http://homepage.divms.uiowa.edu/~jones/bcd/mod.shtml), which is similar in concept to the classic "sum the digits" approach to testing divisibility by 3. This was originally intended for use by processors lacking modulo hardware but is equally at home in hardware (and was covered [here](https://www.eetimes.com/modular-arithmetic-a-divisive-issue/)).

Another optimisation came from the calculation of sum hashes. As this depends on the state of the sum hash in the previous cycle, it can't really be pipelined. Given the previous value `h` and input char `c`, the next value `h' = (h * FNVPrime) XOR c`.

Just letting Quartus figure out the best way to perform the multiplication produces a DSP block, which reduces performance somewhat. But the value of `FNVPrime` is fairly sparse (0x01000193). So I replaced the multiplication with a series of bit-shifts and additions, providing another improvement in FMax.

Related techniques are already widely used in good software compilers; testing for divisibility by a constant is often compiled to a multiplication by a magic number, from which the upper bits are taken and tested further. I'm surprised that, even on the most aggressive performance settings, Quartus doesn't try to provide a solution optimised for an FPGA that can avoid a (usually slower) DSP block being used.

