/*
 *  A module to compute the difference metric between two provided hash bitstrings, based on the
 *  modified Levenshtein distance calculation performed by ssdeep
 *
 *  Inputs:
 *  - hash1_in, hash2_in: two t_var_hashes
 *  - txn_id_in: a tag attached to this set of inputs
 *  - input_valid: pulsed true for one cycle to begin computation on the hashes, latching inputs
 *
 *  Outputs:
 *  - ready: asserted when inputs can be captured on the next clock edge
 *  - hash1_out, hash2_out: the inputs used to produce this result
 *  - txn_id_out: the transaction's assigned tag
 *  - edit_dist: the computed edit distance score of the hashes
 *  - output_valid: pulsed high for one cycle once edit distance has been computed
 *
 *  Warning: the pipeline may (read: will) cause the re-ordering of inputs when:
 *  (a) The input hashes are equal. The result will be presented after a single trip through.
 *  (b) The input hash1s differ in length. Shorter hash1s are processed in fewer pipeline cycles.
 *
 *  Performance: achieves one iteration of the edit distance algorithm per clock. Minimum throughput
 *  is one comparison every MaxDigestLength cycles.
 *
 *  Implementation:
 *  This is implemented using a configurable-length pipeline. On every clock cycle, the last element
 *  of the pipeline is inspected. If it's been iterated over, is a bubble, or its value is already
 *  known, it is removed and its results presented. If this is the case, a new pipelined request can
 *  be pushed into the pipeline; failing that, a bubble is pushed instead.
 *  If the final element of the pipeline is one that requires further computation, it's returned to
 *  the head of the pipeline intact, and no inputs are accepted.
 *  No matter what, every element of the pipeline is advanced one place on every clock cycle.
 *  To provide an output, its hash1 and hash2 values are presented, along with its computed edit
 *  distance, and an output valid signal.
 *  Note that the ready signal is computed via assign statement using the first condition. It
 *  indicates if, on the next clock cycle, the inputs will be sampled. When a user sees the ready
 *  signal go high, it should assert any waiting request and input_valid for the next cycle only.
 *  For instance, the following will work correctly:
 *  if (hc.ready && has_request_ready)
 *      hash1 <= ...; hash2 <= ...; input_valid <= 1
 *  else
 *      input_valid <= 0;
 */

import common::*;
import edit_dist_pipeline_common::*;

module hash_comparator (
    input  wire logic  clk,
    input  wire logic  rst,

    input  t_var_hash  hash1_in,
    input  t_var_hash  hash2_in,
    input  t_txn_id    txn_id_in,
    input  wire logic  input_valid,

    output logic       ready,
    output t_var_hash  hash1_out,
    output t_var_hash  hash2_out,
    output t_txn_id    txn_id_out,
    output t_edit_dist edit_dist,
    output logic       output_valid
);
    t_pipeline_state pipeline_in;
    t_pipeline_state pipeline_out;
    t_pipeline_state pipeline_penultimate;
    edit_dist_pipeline pipeline_module (
        .clk(clk),
        .rst(rst),
        .in(pipeline_in),
        .out(pipeline_out),
        .penultimate(pipeline_penultimate)
    );
    
    assign ready = state_is_complete(pipeline_penultimate);
    
    always_ff @(posedge clk or posedge rst) begin
        if (rst) begin
            output_valid <= 0;
        end else begin
            // Handle final element in pipeline
            hash1_out  <= pipeline_out.hash1;
            hash2_out  <= pipeline_out.hash2;
            txn_id_out <= pipeline_out.txn_id;
            // TODO: could clean this up a bit if functions worked as in simulation
            unique case (pipeline_out.e_type)
                BUBBLE: begin
                    // Nothing to output.
                    edit_dist    <= 'x;
                    output_valid <= 0;
                    pipeline_in  <= state_new_from_inputs(hash1_in, hash2_in, input_valid);
                end
                
                FASTPATH: begin
                    // Fastpath result: push results out
                    edit_dist    <= pipeline_out.fastpath.fp_edit_dist;
                    output_valid <= 1;
                    pipeline_in  <= state_new_from_inputs(hash1_in, hash2_in, input_valid);
                end
                
                ACTIVE: begin
                    if (pipeline_out.active.hash1_index < pipeline_out.hash1.len + 1) begin
                        // Still needs work
                        edit_dist    <= 'x;
                        output_valid <= 0;
                        pipeline_in  <= pipeline_out;
                    end else begin
                        // Done. Read out edit distance
                        edit_dist    <= (pipeline_out.active.dist_array[0][pipeline_out.hash2.len+1] == 0)
                            ? MaxScore
                            : pipeline_out.active.dist_array[0][pipeline_out.hash2.len+1];
                        output_valid <= 1;
                        pipeline_in  <= state_new_from_inputs(hash1_in, hash2_in, input_valid);
                    end
                end
            endcase
        end
    end

    // Determine if the current inputs can be fastpathed
    function logic can_fastpath(input t_var_hash f_hash1, f_hash2);
        //return hash_eq(f_hash1, f_hash2);
        logic [MaxDigestLength-1:0] ok_bits;
        // Only compare parts of var_hashes that are in range
        for (int i = 0; i < MaxDigestLength; i++)
            ok_bits[i] = (f_hash1.digest[i] == f_hash2.digest[i]) || i >= (f_hash1.len + 1);
        return (f_hash1.len == f_hash2.len) && (&ok_bits);
    endfunction
    
    // Determine if a pipeline stage is complete, after it reaches the end
    function automatic logic state_is_complete(input t_pipeline_state state);
        unique case (state.e_type)
            // Bubbles and fastpaths are always complete
            BUBBLE:   return 1;
            FASTPATH: return 1;
            // Actives are complete if hash1_index is on its last value
            ACTIVE:   return (state.active.hash1_index == state.hash1.len);
        endcase
    endfunction

    // Compute the edit distance score for this pipeline state
    function t_edit_dist state_extract_distance(input t_pipeline_state state);
        assert(state.e_type != BUBBLE);
        unique case (state.e_type)
            // Fastpath: is pre-computed
            FASTPATH: return state.fastpath.fp_edit_dist;
            // Active: derive from dist_array
            ACTIVE: return (state.active.dist_array[state.hash2.len + 1] == 0)
                ? MaxScore
                : state.active.dist_array[state.hash2.len + 1];
        endcase
    endfunction

    // Return the initial value of the dist array
    function t_dist_array initial_dist_array();
        for (int i = 0; i < MaxDigestLength + 1; i++) begin
            initial_dist_array[0][i] = i * DeleteCost;
            initial_dist_array[1][i] = 'x;
        end
        return initial_dist_array;
    endfunction

    // Return a pipeline stage for the current set of inputs
    function t_pipeline_state state_new_from_inputs(input t_var_hash f_hash1_in, f_hash2_in,
        input logic f_input_valid);
        
        logic [MaxDigestLength-1:0] ok_bits;
        logic is_equal;
        // Only compare parts of var_hashes that are in range
        // TODO: the below always returns 0? Likely Quartus bug
        //for (int i = 0; i < MaxDigestLength; i++)
        //    ok_bits[i] = (f_hash1_in.digest[i] == f_hash2_in.digest[i]) || i >= (f_hash1_in.len + 1);
        //is_equal = (f_hash1_in.len == f_hash2_in.len) && (&ok_bits);
        
        if (f_input_valid)
            //if (can_fastpath(f_hash1_in, f_hash2_in))
            // if (is_equal)
            // This bodge requires the user to 0-pack unused input
            if (f_hash1_in == f_hash2_in)
                return '{
                    e_type: FASTPATH,
                    txn_id: txn_id_in,
                    hash1:  f_hash1_in,
                    hash2:  f_hash2_in,
                    fastpath: '{
                        fp_edit_dist: 0
                    },
                    default: 'x
                };
            else
                return '{
                    e_type: ACTIVE,
                    txn_id: txn_id_in,
                    hash1:  f_hash1_in,
                    hash2:  f_hash2_in,
                    active: '{
                        hash1_index: 0,
                        dist_array:  initial_dist_array()
                    },
                    default: 'x
                };
        else
            return '{
                e_type: BUBBLE,
                txn_id: txn_id_in,
                default: 'x
            };
    endfunction

endmodule
