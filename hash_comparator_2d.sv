/*
 *  An enhanced version of the hash comparator using a full-size edit distance array
 *  This implementation works in diagonal stripes across the array which should reduce logic usage
 *  substantially, and ensure digests only need a single trip through the pipeline, and all while
 *  supporting similar clockspeeds to the maximally-pipelined implementation!
 */

import common::*;

module hash_comparator_2d (
    input  wire logic  clk,
    input  wire logic  reset,

    input  t_var_hash  hash1_in,
    input  t_var_hash  hash2_in,
    input  wire logic  input_valid,

    output t_var_hash  hash1_out,
    output t_var_hash  hash2_out,
    output t_edit_dist edit_dist,
    output logic       output_valid
);

typedef struct {
    logic       is_valid;
    t_var_hash  hash1;
    t_var_hash  hash2;
    t_edit_dist edit_dist_result;
} t_maybe_edit_dist;

// The full edit distance array
t_edit_dist [MaxDigestLength-1:0][MaxDigestLength-1:0] edit_dist_array;
// A buffer of inputs and results that can be generated from each slice of the array
t_maybe_edit_dist data_fifo [MaxDigestLength+1];

// Lookup an edit distance score from the array, generating constant values implicitly
function t_edit_dist get_score(input int x, y);
    if (x == 0)
        return y;
    else if (y == 0)
        return x;
    else
        return edit_dist_array[x - 1][y - 1];
endfunction

// Returns whether the hashes being compared in slice_number match at positions x and y
function logic is_match(input int x, y, slice_number);
    if (slice_number == 0)
        return hash1_in.digest[x] == hash2_in.digest[y];
    else
        return data_fifo[slice_number - 1].hash1.digest[x] == data_fifo[slice_number - 1].hash2.digest[y];
endfunction

// Compute the score for the given cell, based on the current state of edit_dist_array.
// Used for the first diagonal of cells in a slice
function t_edit_dist compute_score_first(input int x, y, slice_number);
    if (x == -1 || y == -1) return get_score(x + 1, y + 1);

    // Check inputs in range and x, y correct for slice
    assert(x >= 0 && y >= 0 && x < MaxDigestLength && y < MaxDigestLength);
    assert(slice_number >= 0 && slice_number <= MaxDigestLength);

    return min3(
        get_score(x    , y + 1) + InsertCost,
        get_score(x + 1, y    ) + DeleteCost,
        get_score(x    , y    ) + (is_match(x, y, slice_number) ? 0 : ReplaceCost)
    );
endfunction

// Computes results on the second diagonal of a slice
function t_edit_dist compute_score_second(input int x, y, slice_number);
    // Check inputs in range and x, y correct for slice
    assert(x >= 0 && y >= 0 && x < MaxDigestLength && y < MaxDigestLength);
    assert(slice_number >= 0 && slice_number <= MaxDigestLength);

    return min3(
        compute_score_first(x - 1, y    , slice_number) + InsertCost,
        compute_score_first(x    , y - 1, slice_number) + DeleteCost,
        get_score(x, y) + (is_match(x, y, slice_number) ? 0 : ReplaceCost)
    );
endfunction

// Compute the next value for edit_dist_array[x][y]
function t_edit_dist compute_score(input int x, y);
    if (((x + y) % 2) == 0)
        // Is a first slice
        return compute_score_first(x, y, (x + y) / 2);
    else
        // Is a second slice
        return compute_score_second(x, y, (x + y - 1) / 2);
endfunction

// Check to see if the corresponding slice contains the solution
function logic does_slice_contain_result(input int slice_number);
    if (data_fifo[slice_number].is_valid)
        return data_fifo[slice_number].hash1.len + data_fifo[slice_number].hash2.len == 2 * slice_number
            || data_fifo[slice_number].hash1.len + data_fifo[slice_number].hash2.len == 2 * slice_number + 1;
    else
        return 0;
endfunction

// Retrieve the solution from the given slice, assuming it contains a solution!
function t_edit_dist get_solution(input int slice_number);
    return get_score(
        data_fifo[slice_number].hash1.len + 1,
        data_fifo[slice_number].hash2.len + 1
    );
endfunction

always_ff @(posedge clk or posedge reset)
    if (reset) begin
        // Don't care edit_dist_array initialisation, but set all data_fifo items to invalid
        for (int i = 0; i <= MaxDigestLength; i++)
            data_fifo[i] <= '{is_valid:0, default: 'x};
    end else begin
        // Handle data_fifo
        data_fifo[0] <= '{
            is_valid: input_valid,
            hash1: hash1_in,
            hash2: hash2_in,
            edit_dist_result: 'x
        };

        for (int i = 1; i <= MaxDigestLength; i++) begin
            data_fifo[i].is_valid <= data_fifo[i - 1].is_valid;
            data_fifo[i].hash1    <= data_fifo[i - 1].hash1;
            data_fifo[i].hash2    <= data_fifo[i - 1].hash2;
            data_fifo[i].edit_dist_result <= does_slice_contain_result(i - 1) ?
                get_solution(i - 1) : data_fifo[i - 1].edit_dist_result;
        end

        // Handle outputs from data fifo
        hash1_out    <= data_fifo[MaxDigestLength].hash1;
        hash2_out    <= data_fifo[MaxDigestLength].hash2;
        edit_dist    <= data_fifo[MaxDigestLength].edit_dist_result;
        output_valid <= data_fifo[MaxDigestLength].is_valid;

        // Handle intermediate array slices
        for (int x = 0; x < MaxDigestLength; x++)
            for (int y = 0; y < MaxDigestLength; y++)
                edit_dist_array[x][y] <= compute_score(x, y);
    end


endmodule
