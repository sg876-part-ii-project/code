/*
 *  A module that takes bytes as input, and produces a spamsum as output.
 *  Intended for use when the input can be retried, so block_size_number is a runtime parameter.
 */
import common::*;

module hash_state (
    input logic clk,
    input logic reset,

    input t_byte input_byte,
    input logic input_byte_valid,
    input logic stream_start,
    input logic stream_end,
    input t_block_size_number block_size_number,

    output t_var_hash result,
    output t_half_var_hash half_result,
    output logic result_long_enough,
    output logic result_valid
);
    // Components of the rolling hash r
    typedef t_byte [WindowSize-1:0] t_window;
    typedef logic [$clog2(WindowSize)-1:0] t_window_ctr;
    typedef struct {
        integer unsigned h1;
        integer unsigned h2;
        integer unsigned h3;
        t_window         window;
        t_window_ctr     n;
    } t_roll_hash_state;

    // Define pipeline stages
    // Stage 1: calculates bitmask and updates roll state
    struct {
        t_byte byte_in;
        logic byte_valid;
        logic first_byte;
        logic last_byte;
        t_roll_hash_state roll_state;
        integer unsigned bitmask;
    } stage_1;

    // Stage 2: computes roll hash value from stage 1 state
    struct {
        t_byte byte_in;
        logic byte_valid;
        logic first_byte;
        logic last_byte;
        integer unsigned roll_hash;
        integer unsigned bitmask;
    } stage_2_pl, stage_2_pl2, stage_2;

    // Stage 3: computes whether roll hash has reached a reset point
    struct {
        t_byte byte_in;
        logic byte_valid;
        logic first_byte;
        logic last_byte;
        logic is_reset;
    } stage_3_pl, stage_3;

    // Stage 4: updates sum hash, presenting both pre- and post-reset values
    struct {
        logic byte_valid;
        logic first_byte;
        logic last_byte;
        t_hash_len result_len;
        integer unsigned sum_hash;
        integer unsigned sum_hash_before_reset;
        integer unsigned sum_hash_2;
        integer unsigned sum_hash_2_before_reset;
        logic is_reset;
    } stage_4;

    // Stage 5: pushes sum hash values into result on reset
    struct {
        logic byte_valid;
        logic last_byte;
        integer unsigned sum_hash;
        integer unsigned sum_hash_2;
        t_var_hash partial_result;
        t_half_var_hash partial_half_result;
        logic has_partial_results;
    } stage_5;

    always_ff @(posedge clk or posedge reset)
        if (reset) begin
            // Reset pipeline state
            stage_1 <= '{
                byte_in: 0,
                byte_valid: 0,
                first_byte: 0,
                last_byte: 0,
                roll_state: initial_rh(),
                bitmask: 0
            };
            
            stage_2_pl <= '{
                byte_in: 0,
                byte_valid: 0,
                first_byte: 0,
                last_byte: 0,
                roll_hash: 0,
                bitmask: 0
            };
            
            stage_2_pl2 <= '{
                byte_in: 0,
                byte_valid: 0,
                first_byte: 0,
                last_byte: 0,
                roll_hash: 0,
                bitmask: 0
            };

            stage_2 <= '{
                byte_in: 0,
                byte_valid: 0,
                first_byte: 0,
                last_byte: 0,
                roll_hash: 0,
                bitmask: 0
            };

            stage_3_pl <= '{
                byte_in: 0,
                byte_valid: 0,
                first_byte: 0,
                last_byte: 0,
                is_reset: 0
            };
            
            stage_3 <= '{
                byte_in: 0,
                byte_valid: 0,
                first_byte: 0,
                last_byte: 0,
                is_reset: 0
            };

            stage_4 <= '{
                byte_valid: 0,
                first_byte: 0,
                last_byte: 0,
                result_len: 0,
                sum_hash: initial_sh(),
                sum_hash_before_reset: initial_sh(),
                sum_hash_2: initial_sh(),
                sum_hash_2_before_reset: initial_sh(),
                is_reset: 0
            };

            stage_5 <= '{
                byte_valid: 0,
                last_byte: 0,
                sum_hash: 0,
                sum_hash_2: 0,
                partial_result: initial_vh(),
                partial_half_result: initial_hvh(),
                has_partial_results: 0
            };

            result <= initial_vh();
            half_result <= initial_hvh();
            result_long_enough <= 'x;
            result_valid <= 0;
        end else begin
            // Stage 1: update roll_state according to input byte and generate bitmask
            stage_1 <= '{
                byte_in: input_byte,
                byte_valid: input_byte_valid,
                first_byte: input_byte_valid && stream_start,
                last_byte: input_byte_valid && stream_end,
                roll_state: input_byte_valid ?
                    update_rh(stream_start ? initial_rh() : stage_1.roll_state, input_byte) :
                    stage_1.roll_state,
                bitmask: (input_byte_valid && stream_start) ? (1 << block_size_number) - 1 : stage_1.bitmask
            };

            // Stage 2: calculate the roll hash integer using h1, h2, and h3
            stage_2_pl <= '{
                byte_in: stage_1.byte_in,
                byte_valid: stage_1.byte_valid,
                first_byte: stage_1.first_byte,
                last_byte: stage_1.last_byte,
                roll_hash: stage_1.roll_state.h1 + stage_1.roll_state.h2 + stage_1.roll_state.h3,
                bitmask: stage_1.bitmask
            };
            // Add some NOP stages to give Quartus some timing flexibility
            stage_2_pl2 <= stage_2_pl;
            stage_2 <= stage_2_pl2;

            // Stage 3: test if this roll hash integer is a reset point, provided is a valid byte
            stage_3_pl <= '{
                byte_in: stage_2.byte_in,
                byte_valid: stage_2.byte_valid,
                first_byte: stage_2.first_byte,
                last_byte: stage_2.last_byte,
                is_reset: stage_2.byte_valid && is_trigger(stage_2.roll_hash, stage_2.bitmask)
            };
            // Give Quartus another NOP stage to help timing
            stage_3 <= stage_3_pl;

            // Stage 4: update sum hash if byte_valid, resetting if required
            stage_4 <= '{
                byte_valid: stage_3.byte_valid,
                first_byte: stage_3.first_byte,
                last_byte: stage_3.last_byte,
                result_len: stage_3.first_byte ? 0 :
                    (stage_3.is_reset && stage_4.result_len < MaxDigestLength - 1) ?
                        stage_4.result_len + 1 :
                        stage_4.result_len,
                sum_hash: stage_3.byte_valid ?
                    update_sh(
                        stage_3.first_byte ? initial_sh() : stage_4.sum_hash,
                        stage_3.byte_in, stage_3.is_reset && stage_4.result_len < MaxDigestLength - 1
                    ) : stage_4.sum_hash,
                sum_hash_before_reset: stage_3.byte_valid ?
                    update_sh(
                        stage_3.first_byte ? initial_sh() : stage_4.sum_hash,
                        stage_3.byte_in, 0
                    ) : stage_4.sum_hash_before_reset,
                sum_hash_2: stage_3.byte_valid ?
                    update_sh(
                        stage_3.first_byte ? initial_sh() : stage_4.sum_hash_2,
                        stage_3.byte_in, stage_3.is_reset && stage_4.result_len < MaxDigestLength/2 - 1
                    ) : stage_4.sum_hash_2,
                sum_hash_2_before_reset: stage_3.byte_valid ?
                    update_sh(
                        stage_3.first_byte ? initial_sh() : stage_4.sum_hash_2,
                        stage_3.byte_in, 0
                    ) : stage_4.sum_hash_2_before_reset,
                is_reset: stage_3.is_reset
            };

            // Stage 5: if reset occurred, push char into output
            stage_5.byte_valid      <= stage_4.byte_valid;
            stage_5.last_byte       <= stage_4.last_byte;
            stage_5.sum_hash        <= stage_4.sum_hash;
            stage_5.sum_hash_2      <= stage_4.sum_hash_2;
            if (stage_4.is_reset) begin
                // TODO: using push_result here causes assignments to stop working - likely Quartus bug.
                if (stage_4.first_byte) begin
                    // Ensure correct values used for reset hit on first byte
                    stage_5.partial_result.digest[0] <= 6'(stage_4.sum_hash_before_reset);
                    stage_5.partial_result.len <= 0;
                    stage_5.partial_half_result.digest[0] <= 6'(stage_4.sum_hash_2_before_reset);
                    stage_5.partial_half_result.len <= 0;
                end else begin
                    if (stage_5.partial_result.len == MaxDigestLength - 1) begin
                        // Partial result is full
                        stage_5.partial_result.digest[
                            stage_5.partial_result.len
                        ] <= 6'(stage_4.sum_hash_before_reset);
                    end else begin
                        // Partial result not full
                        stage_5.partial_result.digest[
                            stage_5.partial_result.len + stage_5.has_partial_results
                        ] <= 6'(stage_4.sum_hash_before_reset);
                        stage_5.partial_result.len <= 6'(stage_5.partial_result.len + stage_5.has_partial_results);
                    end

                    if (stage_5.partial_half_result.len == MaxDigestLength/2 - 1) begin
                        // Half-size partial result full
                        stage_5.partial_half_result.digest[
                            stage_5.partial_half_result.len
                        ] <= 6'(stage_4.sum_hash_2_before_reset);
                    end else begin
                        // Half-size partial result not full
                        stage_5.partial_half_result.digest[
                            stage_5.partial_half_result.len + stage_5.has_partial_results
                        ] <= 6'(stage_4.sum_hash_2_before_reset);
                        stage_5.partial_half_result.len <= 6'(stage_5.partial_half_result.len + stage_5.has_partial_results);
                    end
                end
                // Indicate first b64char of partial results has been filled
                stage_5.has_partial_results <= 1;
            end else if (stage_4.first_byte) begin
                stage_5.partial_result <= initial_vh();
                stage_5.partial_half_result <= initial_hvh();
                stage_5.has_partial_results <= 0;
            end

            // Output stage: handle last byte and push outputs
            // Should test if roll hash was 0 here for full compatibility (and not push a new value
            // if so), but under assumption that all streams are at least one byte, this should
            // diverge in around one in four billion cases (and only in the last b64_char)
            if (stage_5.last_byte) begin
                // If already full, overwrite final b64char; else push new b64char
                if (stage_5.partial_result.len == MaxDigestLength - 1) begin
                    result.digest[MaxDigestLength - 1] <= stage_5.sum_hash[5:0];
                    result.digest[MaxDigestLength - 2:0] <= stage_5.partial_result.digest[MaxDigestLength - 2:0];
                    result.len <= stage_5.partial_result.len;
                end else begin
                    // Copy and push new result
                    for (int i = 0; i < MaxDigestLength; i++) begin
                        if (i == clamp(stage_5.partial_result.len + stage_5.has_partial_results, 0, MaxDigestLength - 1))
                            result.digest[i] <= stage_5.sum_hash[5:0];
                        else
                            result.digest[i] <= stage_5.partial_result.digest[i];
                    end
                    result.len <= stage_5.partial_result.len + 1;
                end


                if (stage_5.partial_half_result.len == MaxDigestLength/2 - 1) begin
                    half_result.digest[MaxDigestLength/2 - 1] <= stage_5.sum_hash_2[5:0];
                    half_result.digest[MaxDigestLength/2 - 2:0] <= stage_5.partial_half_result.digest[MaxDigestLength/2 - 2:0];
                    half_result.len <= stage_5.partial_half_result.len;
                end else begin
                    // Copy and push new result
                    for (int i = 0; i < MaxDigestLength/2; i++) begin
                        if (i == clamp(stage_5.partial_half_result.len + stage_5.has_partial_results, 0, MaxDigestLength/2 - 1))
                            half_result.digest[i] <= stage_5.sum_hash_2[5:0];
                        else
                            half_result.digest[i] <= stage_5.partial_half_result.digest[i];
                    end
                    half_result.len <= stage_5.partial_half_result.len + 1;
                end
            end else begin
                result      <= initial_vh();
                half_result <= initial_hvh();
            end
            // TODO: this also breaks. Thanks Quartus?
            // result <= (stage_5.last_byte) ?
            //     push_result(stage_5.partial_result, stage_5.sum_hash) : initial_vh();
            // half_result <= (stage_5.last_byte) ?
            //     push_half_result(stage_5.partial_half_result, stage_5.sum_hash_2) : initial_hvh();
            result_long_enough <= (stage_5.partial_result.len + stage_5.has_partial_results + 1) > MaxDigestLength / 2;
            result_valid <= stage_5.last_byte && stage_5.byte_valid;
        end

    // Push the lower 6 bits of a sum hash into a var_hash and half_var_hash
    function automatic t_var_hash push_result(input t_var_hash vh_in, input integer unsigned sum_hash);
        t_var_hash vh_out;
        vh_out = vh_in;
        vh_out.digest[vh_out.len] = sum_hash[5:0];
        if (vh_out.len != MaxDigestLength - 1) vh_out.len++;
        return vh_out;
    endfunction
    function automatic t_half_var_hash push_half_result(input t_half_var_hash hvh_in, input integer unsigned sum_hash);
        t_half_var_hash hvh_out;
        hvh_out = hvh_in;
        hvh_out.digest[hvh_out.len] = sum_hash[5:0];
        if (hvh_out.len != (MaxDigestLength/2) - 1) hvh_out.len++;
        return hvh_out;
    endfunction

    function automatic integer unsigned update_sh(
        input integer unsigned s, input t_byte c, input logic is_reset
    );
        if (is_reset)
            return FNVOffsetBasis;
        else
            //return (s * FNVPrime) ^ c;
            return fnv_mult(s) ^ c;
    endfunction

    function automatic integer unsigned initial_sh();
        return FNVOffsetBasis;
    endfunction

    function automatic t_roll_hash_state update_rh(input t_roll_hash_state r, input t_byte c);
        t_roll_hash_state result;
        // Update h1, h2, h3
        result.h1 = r.h1 + c - r.window[r.n];
        result.h2 = r.h2 + WindowSize * c - r.h1;
        result.h3 = (r.h3 << H3Shift) ^ c;
        // Update window
        result.window = r.window;
        result.window[r.n] = c;
        // Update n % 7
        result.n = (r.n == WindowSize - 1) ? 0 : r.n + 1;
        return result;
    endfunction

    function automatic t_roll_hash_state initial_rh();
        return '{
            h1: 0,
            h2: 0,
            h3: 0,
            window: '{default: 0},
            n: 0
        };
    endfunction

    // Returns 3|a via evil bit level hacking
    // Derived from http://homepage.divms.uiowa.edu/~jones/bcd/mod.shtml
    // Explanation: is derived from the digit root approach to testing divisibility by 3
    function automatic logic divis3(input integer unsigned a);
        a = (a >> 16) + (a & 32'hFFFF);
        a = (a >>  8) + (a &   32'hFF);
        a = (a >>  4) + (a &    32'hF);
        a = (a >>  2) + (a &    32'h3);
        a = (a >>  2) + (a &    32'h3);
        a = (a >>  2) + (a &    32'h3);
        return (a == 3) || (a == 0);
    endfunction

    function automatic logic is_trigger(
        input integer unsigned roll_hash,
        input integer unsigned bitmask
    );
        return (((roll_hash+1) & bitmask) == 0) && divis3(roll_hash+1);
    endfunction
    
    // Returns x * FNVPrime
    function automatic integer fnv_mult(input integer x);
        // Bit-shifts and multiplication: up to 200 MHz
        return (x << 24) + (x << 8) + (x << 7) + (x << 4) + (x << 1) + (x << 0);
        // Naive implementation below uses a DSP block with FMax 175 MHz
        //return x * FNVPrime;
    endfunction

endmodule
