vlog common.sv
vlog verification_common.sv
vlog hash_state.sv
vlog hash_calculator.sv
vlog avalon_hash_calculator.sv
vlog tb_a_calc.sv

vsim work.tb_a_calc

add wave *
add wave /tb_a_calc/ahc/*
