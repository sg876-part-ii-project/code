/*
 *  Contains common datatypes for edit distance pipeline users
 */

import common::*;

package edit_dist_pipeline_common;
    typedef logic [$clog2(common::MaxScore)-1:0] t_score;
    typedef t_score [1:0][common::MaxDigestLength:0] t_dist_array;
    typedef logic [$clog2(common::MaxDigestLength + 1) - 1:0] t_dist_array_idx;

    typedef enum {BUBBLE, ACTIVE, FASTPATH} t_e_type;

    parameter TxnBits = 8;
    typedef logic [TxnBits-1:0] t_txn_id;

    parameter PipelineStages = 23;
    parameter GroupSize = 3;
    // note PipelineStages = ceil(MaxDigestLength+1/GroupSize) + 1; e.g.
    // GroupSize    PipelineStages @ MaxDigestLength = 64
    // 1            66
    // 2            34
    // 3            23
    // 4            18
    // 5            14
    // 6            12

    typedef struct {
        // What type of pipeline entry this is
        // Bubble: self-explanatory. Specified 0 to ensure pipeline initialised to empty.
        //   All fields are invalid for this entry type.
        // Active: an entry that needs to be processed.
        //   All fields are valid for this entry type.
        // Fastpath: an entry where the result can be derived directly from the inputs.
        //   Only the hashn fields are valid for this entry type.
        t_e_type e_type;
        t_txn_id txn_id;

        // Hash data
        common::t_var_hash hash1;
        common::t_var_hash hash2;

        // Data varying by e_type
        // e_type == ACTIVE
        struct {
            // Iteration counter over characters in hash1
            logic [$clog2(common::MaxDigestLength):0] hash1_index;
            // A 2*(MaxDigestLength+1)-length array of edit distance scores
            t_dist_array dist_array;
        } active;

        // e_type == FASTPATH
        struct {
            common::t_edit_dist fp_edit_dist;
        } fastpath;
    } t_pipeline_state;

endpackage
