/*
 *  A module that takes bytes as input, and produces a spamsum as output
 *
 *  Note that all inputs are only sampled on clocks where input_byte_valid = 1.
 *  Inputs:
 *  - input_byte_in: the next byte to update the hash state with
 *  - input_byte_valid: when high, byte_in is sampled every rising clock edge
 *  - input_block_size: block size value to use
 *  - input_stream_start: indicates this byte represents the start of a new stream
 *  - input_stream_end: indicates this byte represents the end of a stream; outputs are valid next cycle.
 *  - input_stream_length: number of input bytes that this stream will consist of. Sampled only
 *    when input_stream_start = 1.
 *
 *  Outputs:
 *  - spamsum: the computed spamsum for the data stream
 *  - output_valid: pulsed high when spamsum asserted
 *  - ready: can accept a new data stream
 *
 *  Other notes:
 *  - Streams of length 1 byte do not generate valid sums; 3:A:A is returned instead of 3::. This is
 *    a result of an internal optimisation that reduces the size of a var_hash slightly.
 *  - Sums of long streams do not meaningfully reflect data from 192 GiB onwards, due to inherent
 *    limitations of the spamsum algorithm.
 *  - Has a fixed delay between asserting input_stream_end for a byte, and receiving the output.
 */

import common::*;

module hash_calculator (
    input  logic        clk,
    input  logic        reset,

    input  t_byte       input_byte_in,
    input  logic        input_byte_valid,
    input  logic        input_stream_start,
    input  logic        input_stream_end,
    input  t_stream_len input_stream_length,

    output t_spamsum    spamsum,
    output logic        spamsum_valid,
    output logic        ready
);

parameter NumModules = 5;
t_var_hash results [NumModules];
t_half_var_hash half_results [NumModules];
logic results_long_enough [NumModules];
logic results_valid [NumModules];
// Modules are kept in sync provided input signals are shared
logic result_valid; assign result_valid = results_valid[0];

t_block_size_number block_size_guess;
t_byte byte_in;
logic byte_valid;
logic stream_start;
logic stream_end;

// In each, index n maps to the output from the module using block size offset by 1 - n
// 0 --> +1 (next block size up), 1 --> 0, 2 --> -1 (half block size), 3 --> -2, etc
// To accomodate up to n possible block size halvings, set NumModules to 2 + n
genvar gen_i;
generate
    for (gen_i = 0; gen_i < NumModules; gen_i++) begin : gen_hs
        hash_state hs (
            .clk(clk),
            .reset(reset),
            .input_byte(byte_in),
            .input_byte_valid(byte_valid),
            .stream_start(stream_start),
            .stream_end(stream_end),
            .block_size_number(clamp(block_size_guess + 1 - gen_i, 0, MaxBlockSize)),
            .result(results[gen_i]),
            .half_result(half_results[gen_i]),
            .result_long_enough(results_long_enough[gen_i]),
            .result_valid(results_valid[gen_i])
        );
    end
endgenerate

always_ff @(posedge clk or posedge reset)
    if (reset) begin
        byte_in        <= 'x;
        byte_valid     <= 0;
        stream_start   <= 'x;
        stream_end     <= 'x;
        spamsum        <= '{default: 'x};
        spamsum_valid  <= 0;
        ready          <= 1;
    end else begin
        // Handle module inputs
        if (input_byte_valid) begin
            if (input_stream_start) block_size_guess <= get_bsn();
            byte_in      <= input_byte_in;
            byte_valid   <= input_byte_valid;
            stream_start <= input_stream_start;
            stream_end   <= input_stream_end;
        end else begin
            // Set explicit "don't cares" for module inputs where possible; nothing to pass
            byte_in      <= 'x;
            byte_valid   <= 0;
            stream_start <= 'x;
            stream_end   <= 'x;
        end

        // Handle module outputs
        if (result_valid) begin
            // If module at position i is fine, take output from i and i - 1.
            // Clamping ensures that small inputs use minimum block size
            $display("Guess was %0d, fle = %0d", block_size_guess, first_long_enough());
            spamsum.block_size <= clamp(block_size_guess - (first_long_enough() - 1), 0, MaxBlockSize);
            spamsum.hash_full  <= results[first_long_enough()];
            spamsum.hash_half  <= half_results[first_long_enough() - 1];
            spamsum_valid      <= 1;
        end else begin
            spamsum        <= '{default: 'x};
            spamsum_valid  <= 0;
        end
        if (result_valid)
            ready <= 1;
        else if (input_byte_valid && input_stream_start)
            ready <= 0;
    end

// Given an input length, compute the maximum block size number for this input
function automatic t_block_size_number get_bsn();
    for (int k = 0; k < MaxBlockSize; k++)
        if (3 * 2**k * MaxDigestLength >= input_stream_length)
            return k;
    return MaxBlockSize;
endfunction

// Provides the index at which the first "long enough" full-length result can be found
// Subtract one to get the required block size offset
function automatic integer first_long_enough();
    for (int i = 1; i < NumModules; i++)
        if (results_long_enough[i] || (block_size_guess + 1 - i == 0))
            return i;
    return NumModules - 1;
endfunction

endmodule
