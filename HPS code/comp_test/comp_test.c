#define _BSD_SOURCE
#include <byteswap.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include "bridges.h"

#define FILL_UNUSED_SPACE 1

// Number of bytes to write at a time, or 0 for straight copy,
// -1 for copy without volatile, -2 for memcpy
// Experimentally: little to no difference seen between 8, 0, -1, -2
#define WRITE_WIDTH 0

#define MEM_DEBUG 0
#define PARSE_DEBUG 0

#define SUM_1_OFFSET   (0x100)
#define SUM_2_OFFSET   (0x180)
#define RESULTS_OFFSET (0x400)
#define CONTROL_OFFSET (0x0FF)
#define RESULT_ADDR    (0xBE)

#define BM_DIST_VALID (0x80000000)
#define BM_INCOMP     (0x40000000)
#define BM_STATUS     (BM_DIST_VALID | BM_INCOMP)
#define BM_EDIT_DIST  (0x000000FF)
#define BM_EVERYTHING (0xFFFFFFFF)

#define mem_debug_print(fmt, ...) \
    do { if (MEM_DEBUG) printf(fmt, ##__VA_ARGS__); } while (0)
#define parse_debug_print(fmt, ...) \
    do { if (PARSE_DEBUG) printf(fmt, ##__VA_ARGS__); } while (0)

typedef struct {
    uint8_t bsn;
    uint8_t fslen;
    uint8_t hslen;
    uint8_t PADDING;
    uint8_t fs_digest[64];
    uint8_t hs_digest[32];
} __attribute__((packed)) accel_spamsum_t;

void assert(int cond, char* msg) {
    if (!cond) {
        printf("Failed assertion \"%s\"\n", msg);
        getchar();
        exit(1);
    }
}

uint8_t parse_b64_char(char in) {
    if (in >= 'A' && in <= 'Z') return (in - 'A');
    if (in >= 'a' && in <= 'z') return (in - 'a' + 26);
    if (in >= '0' && in <= '9') return (in - '0' + 52);
    if (in == '+') return 62;
    if (in == '/') return 63;
    assert(0, "b64 char in range"); return -1;
}

uint8_t read_byte(unsigned int offset) {
    uint8_t result = *(volatile uint8_t *) (h2f_hwb + offset);
    mem_debug_print("Rb 0x%x = %x\n", offset, result);
    return result;
}

void write_byte(unsigned int offset, uint8_t byte) {
    mem_debug_print("Wb 0x%x = %x\n", offset, byte);
    *(volatile uint8_t *) (h2f_hwb + offset) = byte;
}

uint32_t read_word(unsigned int offset) {
    uint32_t result = *(volatile uint32_t *) (h2f_hwb + offset);
    mem_debug_print("Rw 0x%x = %x\n", offset, result);
    return result;
}

void write_word(unsigned int offset, uint32_t word) {
    mem_debug_print("Ww 0x%x = %x\n", offset, word);
    *(volatile uint32_t *) (h2f_hwb + offset) = word;
}

void write_dword(unsigned int offset, uint64_t dword) {
    mem_debug_print("Wd 0x%x = %" PRIx64  "\n", offset, dword);
    *(volatile uint64_t *) (h2f_hwb + offset) = dword;
}

// Parse sum into result, returning 1 on success or 0 on fail
int parse_spamsum(char* sum, accel_spamsum_t* result) {
    parse_debug_print("Scanning over %s\n", sum);
    // Parse out the block size
    unsigned int block_size;
    int scanned_vals = sscanf(sum, "%u", &block_size);
    if (scanned_vals == 0) return 0;
    parse_debug_print("Scanned %u\n", block_size);
    block_size /= 3;
    result->bsn = 0; while (block_size >>= 1) result->bsn++;
    parse_debug_print("bsn = %u\n", result->bsn);

    // Determine where the first : is
    int i = 0;
    while (sum[i] != ':') {
        if (sum[i] == '\0' || i > 115) return 0;
        i++;
    }
    // sum[i] == ':'
    i++;
    // i now points to first char of sum1
    int sum1_start = i;
    parse_debug_print("Found start of sum1 at %d\n", sum1_start);
    // Parse the first sum
    while (sum[i] != ':') {
        if (sum[i] == '\0' || (i - sum1_start) > 63) return 0;
        result->fs_digest[i - sum1_start] = parse_b64_char(sum[i]);
        parse_debug_print("  %d: Parsed %c as %d\n", i - sum1_start, sum[i], parse_b64_char(sum[i]));
        i++;
    }
    result->fslen = i - sum1_start - 1;
    parse_debug_print("  Length was %u\n", result->fslen);
    // Fill the remaining space in the digest, if required
    if (FILL_UNUSED_SPACE) {
        for (int j = result->fslen + 2; j < 64; j++) {
            result->fs_digest[j] = 0x00;
            parse_debug_print("Filled unused index %d\n", j);
        }
    }
    // sum[i] == ':' again
    i++;
    // i now points to first char of sum2
    int sum2_start = i;
    parse_debug_print("Found start of sum2 at %d\n", sum2_start);
    while (sum[i] != '\0') {
        if ((i - sum2_start) > 31) return 0;
        result->hs_digest[i - sum2_start] = parse_b64_char(sum[i]);
        parse_debug_print("  %d: Parsed %c as %d\n", i - sum2_start, sum[i], parse_b64_char(sum[i]));
        i++;
    }
    if (FILL_UNUSED_SPACE) {
        for (int j = result->hslen + 2; j < 32; j++) {
            result->hs_digest[j] = 0x00;
            parse_debug_print("Filled unused index %d\n", j);
        }
    }
    result->hslen = i - sum2_start - 1;
    parse_debug_print("  Length was %u\n", result->hslen);
    return 1;
}

void write_sum(unsigned int offset, accel_spamsum_t sum) {
    // Offers several implementations, of which one chosen at compile-time
    if (WRITE_WIDTH == 8) {
        // Get a dword view of the sum
        uint64_t* dword_view = (void*) &sum;
        // Write out as much as possible - the first 96 bytes
        for (int i = 0; i < sizeof(accel_spamsum_t) / 8; i++) {
            write_dword(offset + 8 * i, dword_view[i]);
        }
        // Deal with the remaining 4 bytes
        uint32_t* word_view = (void*) &sum;
        write_word(offset + 96, word_view[24]);
    } else if (WRITE_WIDTH == 4) {
        // Break the type system: get a word-by-word view of the sum
        uint32_t* word_view = (void*) &sum;

        // Write each word out to the accelerator
        for (int i = 0; i < sizeof(accel_spamsum_t) / 4; i++) {
            write_word(offset + 4 * i, word_view[i]);
        }
    } else if (WRITE_WIDTH == 1) {
        uint8_t* byte_view = (void*) &sum;
        for (int i = 0; i < sizeof(accel_spamsum_t); i++) {
            write_byte(offset + i, byte_view[i]);
        }
    } else if (WRITE_WIDTH == 0) {
        *((volatile accel_spamsum_t*) (h2f_hwb + offset)) = sum;
    } else if (WRITE_WIDTH == -1) {
        *((accel_spamsum_t*) (h2f_hwb + offset)) = sum;
    } else if (WRITE_WIDTH == -2) {
        memcpy((void*) (h2f_hwb + offset), &sum, sizeof(sum));
    } else {
        printf("Invalid WRITE_WIDTH of %d\n", WRITE_WIDTH);
        exit(1);
    }
}

struct timeval total_write_time = {0};

// Test the accelerator produces a given result value (within masked-out bits) comparing given sums
void do_test(char* sum1_str, char* sum2_str, uint32_t expect, uint32_t expect_mask) {
    printf("Testing %s vs %s, expecting 0x%x\n", sum1_str, sum2_str, expect);
    printf("Result register is currently 0x%x\n", read_word(RESULTS_OFFSET + 0x00));
    //printf("Press enter to start"); getchar();

    accel_spamsum_t sum1;
    assert(parse_spamsum(sum1_str, &sum1), "Parse sum1_str");

    accel_spamsum_t sum2;
    assert(parse_spamsum(sum2_str, &sum2), "Parse sum2_str");

    struct timeval begin; gettimeofday(&begin, NULL);

    write_sum(SUM_1_OFFSET, sum1);
    write_sum(SUM_2_OFFSET, sum2);

    // Trigger write
    write_byte(CONTROL_OFFSET, RESULT_ADDR);
    struct timeval write_done; gettimeofday(&write_done, NULL);

    // Wait for completion
    uint32_t result;
    while (((result = read_word(RESULTS_OFFSET + 4 * RESULT_ADDR)) & BM_STATUS) == 0) {
        usleep(100);
    }

    struct timeval end; gettimeofday(&end, NULL);

    // Describe results
    printf("Result register is 0x%x\n", result);
    printf("Distance available? %c\n", (result & BM_DIST_VALID) ? 'Y' : 'N');
    printf("Block sizes comparable? %c\n", (result & BM_INCOMP) ? 'N' : 'Y');
    printf("Edit dist: %u\n", result & BM_EDIT_DIST);
    if ((result & expect_mask) != (expect & expect_mask)) {
        printf("Failed this test.\n"); getchar();
    }
    struct timeval write_time; timersub(&write_done, &begin, &write_time);
    printf("Write time: %ld.%06ld\n", write_time.tv_sec, write_time.tv_usec);
    struct timeval wait_time; timersub(&end, &write_done, &wait_time);
    printf("Wait time:  %ld.%06ld\n", wait_time.tv_sec, wait_time.tv_usec);
    printf("========\n\n");
    timeradd(&total_write_time, &write_time, &total_write_time);
}

void do_test_expect_dist(char* sum1, char* sum2, uint8_t expect) {
    do_test(sum1, sum2, BM_DIST_VALID | expect, BM_EVERYTHING);
}

void do_test_expect_fail(char* sum1, char* sum2) {
    do_test(sum1, sum2, BM_INCOMP, BM_STATUS);
}

int main() {
    setbuf(stdout, NULL);
    assert(sizeof(accel_spamsum_t) == 0x64, "accel_spamsum_t size = 0x64 = 100");

    if (open_bridges() == -1) return -1;

    // Non-comparable digests: differ in block size number by more than 1
    do_test_expect_fail("3:+++:++", "12:+++:++");
    do_test_expect_fail("12:+++:++", "3:+++:++");
    do_test_expect_fail("3:+++:++", "3221225472:+++:++");

    // Basic tests
    do_test_expect_dist("3:AAA:++", "3:AAAA:++", 1);
    do_test_expect_dist("6:AAA:++", "3:+++:AAA", 0);
    do_test_expect_dist("3:AAA:++", "3:ABA:++",  2);
    do_test_expect_dist("3:AAA:++", "3:ABBA:++", 3);
    do_test_expect_dist("3:abcd:++", "3:0abcd0:++", 2);
    do_test_expect_dist(
        "3:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA:+",
        "3:BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB:+",
        128
    );
    // Byte-order test
    // correct sees 0123 4567 8901 vs 0012 3456 7890 ==> 2 (1 insertion, 1 removal)
    // wrong sees   3210 7654 1098 vs 2100 6543 0987 ==> 4 (two substitutions, 1 insertion, 1 removal)
    do_test_expect_dist("3:012345678901:+", "3:001234567890:+", 2);

    close_bridges();

    printf("Total write time: %ld.%06ld\n", total_write_time.tv_sec, total_write_time.tv_usec);
}
