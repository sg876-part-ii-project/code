# Script to compile the hash comparator and its test bench, then run the test bench
vlib work

vlog common.sv
vlog verification_common.sv
vlog edit_dist_pipeline_common.sv
vlog edit_dist_pipeline.sv
vlog hash_comparator.sv
vlog tb_hash_comparator.sv

vsim work.tb_hash_comparator

#add wave -position end -radix unsigned *
add wave -position end -radix unsigned sim:/tb_hash_comparator/hc/*
add wave -position end -radix unsigned sim:/tb_hash_comparator/hc/pipeline_module/pipeline

