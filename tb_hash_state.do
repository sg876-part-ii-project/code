vlib work

vlog common.sv
vlog verification_common.sv
vlog hash_state.sv
vlog tb_hash_state.sv

vsim work.tb_hash_state

add wave tb_hash_state/*
